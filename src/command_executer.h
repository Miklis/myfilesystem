//
// Created by miklis on 09.12.20.
//

#ifndef MYFILESYSTEM_COMMAND_EXECUTER_H
#define MYFILESYSTEM_COMMAND_EXECUTER_H
#define COMMAND_BUFF_SIZE 256

return_code execute_command(char *input);


#endif //MYFILESYSTEM_COMMAND_EXECUTER_H
