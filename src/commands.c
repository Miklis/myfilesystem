#include <stdio.h>
#include <printf.h>
#include <string.h>
#include "commands.h"
#include "filesystem.h"
#include "fs_utils.h"
#include "command_executer.h"


/*
  Funkce zkopíreje soubor ze zdrojové cesty do cílového umístění. Je vytvořen nový Inode a jemu jsou přiřazeny nové clustery.

   Proměnné:
            source - zdrojová cesta k souboru
            dest   - nový soubor

   Vrací return_code:
                    CODE_OK              - soubor byl přesunut
                    CODE_FATAL_ERROR     - Při operaci nastala chyba při práci s pamětí
                    CODE_FS_SAME_NAMES   - Nelze přesunou na cílovém místě existuje soubor se stejným jménem
                    CODE_PATH_NOT_FOUND  - Neexistuje cílová cesta
                    CODE_FILE_NOT_FOUND  - Neexistuje zdrojová cesta
 */
return_code cp_func(char *source, char *dest) {
    INODE *src_inode;
    INODE *dest_inode;
    DIR_ITEM *new_dir_item;
    DIR_ITEM *src_dir_item;
    DIR_ITEM *dest_directory;

    char dest_dir_path[MAX_PATH_LEN];
    char dest_file_name[MAX_FILE_NAME];
    char cur_data[CLUSTER_SIZE + 1];

    int inode_id;
    int has_dest_space;
    int same_names;
    int number_of_needed_cluster;
    int processed_data;
    int bytes_to_copy;
    int processed_clusters;

    int32_t offset;
    int32_t *free_clusters;

    return_code result = CODE_OK;

    src_inode = malloc(sizeof(INODE));
    dest_inode = malloc(sizeof(INODE));
    new_dir_item = malloc(sizeof(DIR_ITEM));
    src_dir_item = malloc(sizeof(DIR_ITEM));
    dest_directory = malloc(sizeof(DIR_ITEM));

    if (src_inode == NULL ||
        dest_inode == NULL ||
        new_dir_item == NULL ||
        src_dir_item == NULL ||
        dest_directory == NULL) {
        return CODE_FATAL_ERROR;
    }

    utils_intersect_directory_path_and_file_name(dest, dest_dir_path, dest_file_name);
    result = utils_process_path(source, src_dir_item);
    /* Načtení zdrojového file */
    if (result == CODE_FATAL_ERROR) {
        return result;
    }

    if (result == CODE_OK && strlen(source) != 0) {
        /* Načtení cílové cesty */
        result = utils_process_path(dest_dir_path, dest_directory);
        if (result == CODE_FATAL_ERROR) {
            return result;
        }
        if (result == CODE_OK && strlen(dest_dir_path) != 0) {

            utils_has_dir_space(dest_directory, &offset, &has_dest_space);
            utils_check_dir_items_names(dest_directory, dest_file_name, &same_names);

            if (has_dest_space == 0) {
                if (same_names == 0) {
                    /* Výpočet počtu potřebných clusterů */
                    utils_load_inode(src_inode, src_dir_item->inode);
                    number_of_needed_cluster = utils_calc_needed_cluster_for_file(src_inode->file_size);
                    free_clusters = malloc(number_of_needed_cluster * sizeof(int32_t));
                    if (free_clusters == NULL) {
                        return CODE_FATAL_ERROR;
                    }

                    /* Vyčlenění clusterů do kterých se bude ukládat */
                    if (find_free_clusters(&free_clusters, number_of_needed_cluster) == 0) {
                        inode_id = utils_find_first_free_inode();
                        if (inode_id != -1) {
                            utils_load_inode(src_inode, src_dir_item->inode);
                            dest_inode->nodeid = inode_id;
                            dest_inode->file_size = src_inode->file_size;
                            dest_inode->references = src_inode->references;
                            dest_inode->isDirectory = src_inode->isDirectory;

                            /* Přiřazení clusterů inodu */
                            utils_assign_clusters_to_inode(dest_inode, free_clusters, number_of_needed_cluster);
                            fs_seek_set_i(inode_id);
                            fwrite(dest_inode, sizeof(INODE), 1, global_filesystem);

                            processed_data = dest_inode->file_size;
                            processed_clusters = 0;
                            while (processed_data > 0) {
                                if (processed_data >= CLUSTER_SIZE) {
                                    bytes_to_copy = CLUSTER_SIZE;
                                } else {
                                    bytes_to_copy = processed_data;
                                }
                                utils_read_from_links(src_inode, processed_clusters, bytes_to_copy, cur_data);
                                processed_data -= CLUSTER_SIZE;

                                utils_write_byte_to_links(dest_inode, cur_data, processed_clusters, bytes_to_copy);
                                processed_clusters++;
                            }

                            new_dir_item->inode = dest_inode->nodeid;
                            memset(new_dir_item->item_name, '\0', MAX_FILE_NAME);
                            strncpy(new_dir_item->item_name, dest_file_name, strlen(dest_file_name));


                            utils_write_file_reference_to_dir(dest_directory, new_dir_item, &offset);
                        } else {
                            result = CODE_FS_OUT_OF_I;
                        }

                    } else {
                        result = CODE_FS_FULL_FS;
                    }
                    free(free_clusters);
                } else {
                    result = CODE_FS_SAME_NAMES;
                }
            } else {
                result = CODE_FS_FULL_DIR;
            }
        } else {
            result = CODE_PATH_NOT_FOUND;
        }
    } else {
        result = CODE_FILE_NOT_FOUND;
    }

    free(src_inode);
    free(dest_inode);
    free(new_dir_item);
    free(src_dir_item);
    free(dest_directory);

    return result;
}


/*
  Funkce má za úkol přesunout ze zdrojového místa soubor s určitým jménem na nové místo pod novým zadaným jménem

   Proměnné:
            source - zdrojová cesta k souboru
            dest   - nová zdrojová cesta k souboru

   Vrací return_code:
                    CODE_OK              - soubor byl přesunut
                    CODE_FATAL_ERROR     - Při operaci nastala chyba při práci s pamětí
                    CODE_FS_SAME_NAMES   - Nelze přesunou na cílovém místě existuje soubor se stejným jménem
                    CODE_PATH_NOT_FOUND  - Neexistuje cílová cesta
                    CODE_FILE_NOT_FOUND  - Neexistuje zdrojová cesta
 */
return_code mv_func(char *source, char *dest) {
    INODE *file_inode;
    INODE *old_dir_inode;
    DIR_ITEM *file_item;
    DIR_ITEM *old_parent_dir;
    DIR_ITEM *new_parent_dir;
    int32_t file_offset;
    int has_dir_space;
    int same_names;
    char old_file_name[MAX_FILE_NAME];
    char old_path[MAX_PATH_LEN];
    char new_file_name[MAX_FILE_NAME];
    char new_path[MAX_PATH_LEN];
    return_code result = CODE_OK;

    file_inode = malloc(sizeof(INODE));
    old_dir_inode = malloc(sizeof(INODE));
    file_item = malloc(sizeof(DIR_ITEM));
    old_parent_dir = malloc(sizeof(DIR_ITEM));
    new_parent_dir = malloc(sizeof(DIR_ITEM));

    if (file_inode == NULL || old_dir_inode == NULL || file_item == NULL || old_parent_dir == NULL ||
        new_parent_dir == NULL) {
        return CODE_FATAL_ERROR;
    }

    utils_intersect_directory_path_and_file_name(dest, new_path, new_file_name);
    utils_intersect_directory_path_and_file_name(source, old_path, old_file_name);

    result = utils_process_path(source, file_item);
    if (result == CODE_FATAL_ERROR) {
        return result;
    }

    if (result == CODE_OK && strlen(source) != 0) {
        result = utils_process_path(old_path, old_parent_dir);
        if (result == CODE_FATAL_ERROR) {
            return result;
        }
        /* Kontrola že existuje zdrojová cesta */
        if (result == CODE_OK) {
            result = utils_process_path(new_path, new_parent_dir);
            if (result == CODE_FATAL_ERROR) {
                return result;
            }
            /* Kontrola že existuje cílová cesta */
            if (result == CODE_OK && strlen(dest) != 0){
                utils_load_inode(old_dir_inode, old_parent_dir->inode);
                if (utils_remove_dir_reference(NULL, old_dir_inode, file_item->item_name, false) == CODE_FATAL_ERROR) {
                    return CODE_FATAL_ERROR;
                }
                if (utils_has_dir_space(new_parent_dir, &file_offset, &has_dir_space) != CODE_OK) {
                    return CODE_FATAL_ERROR;
                }

                utils_check_dir_items_names(new_parent_dir, new_file_name, &same_names);
                if (same_names == 0) {
                    if (has_dir_space == 0) {
                        utils_load_inode(old_dir_inode, old_parent_dir->inode);
                        strncpy(file_item->item_name, new_file_name, strlen(new_file_name));
                        file_item->item_name[strlen(new_file_name)] = '\0';
                        if (utils_write_file_reference_to_dir(new_parent_dir, file_item, &file_offset) ==
                            CODE_FATAL_ERROR) {
                            return CODE_FATAL_ERROR;
                        }
                    } else {
                        result = CODE_FS_FULL_DIR;
                    }
                } else {
                    result = CODE_FS_SAME_NAMES;
                }
            }else{
                result = CODE_PATH_NOT_FOUND;
            }
        } else {
            result = CODE_FILE_NOT_FOUND;
        }
    } else {
        result = CODE_FILE_NOT_FOUND;
    }

    free(file_inode);
    free(file_item);
    free(old_parent_dir);
    free(new_parent_dir);
    return result;
}


/*
  Funkce odstraní soubor z filesystému

   Proměnné:
            path - zdrojová cesta k souboru

   Vrací return_code:
                    CODE_OK              - Soubor byl smazám
                    CODE_FATAL_ERROR     - Při operaci nastala chyba při práci s pamětí
                    CODE_FILE_NOT_FOUND  - Neexistuje zdrojová cesta
 */
return_code rm_func(char *path) {
    INODE *file_inode;
    INODE *parent_dir_inode;
    DIR_ITEM *file_dir_item;
    DIR_ITEM *file_directory;
    char dir_path[MAX_PATH_LEN];
    char file_name[MAX_FILE_NAME];
    return_code result = CODE_OK;


    file_inode = malloc(sizeof(INODE));
    parent_dir_inode = malloc(sizeof(INODE));
    file_dir_item = malloc(sizeof(file_dir_item));
    file_directory = malloc(sizeof(file_directory));


    if (file_inode == NULL || file_dir_item == NULL || file_directory == NULL) {
        return CODE_FATAL_ERROR;
    }

    result = utils_process_path(path, file_dir_item);
    if (result == CODE_FATAL_ERROR) {
        return result;
    }


        if (result == CODE_OK) {
            utils_intersect_directory_path_and_file_name(path, dir_path, file_name);
            result = utils_process_path(dir_path, file_directory);
            if (result == CODE_FATAL_ERROR) {
                return result;
            }

            utils_load_inode(file_inode, file_dir_item->inode);
            if (file_inode->isDirectory == false) {
                if (result == CODE_OK) {

                    utils_load_inode(parent_dir_inode, file_directory->inode);
                    utils_remove_dir_reference(file_inode, parent_dir_inode, file_name, true);
                    //utils_remove_inode(file_inode);
                } else {
                    result = CODE_FILE_NOT_FOUND;
                }
            }else{
                result = CODE_WRONG_TYPE;
            }
        } else {
            result = CODE_FILE_NOT_FOUND;
        }


    free(file_inode);
    free(parent_dir_inode);
    free(file_dir_item);
    free(file_directory);

    return result;
}


/*
  Funkce vypíše obsah adresáře ke kterému vede předaná cesta

   Proměnné:
            path - zdrojová cesta k adresáři

   Vrací return_code:
                    CODE_OK              - Vše v pořádku
                    CODE_FATAL_ERROR     - Při operaci nastala chyba při práci s pamětí
                    CODE_PATH_NOT_FOUND  - Neexistuje cílová cesta
 */
return_code ls_func(char *path) {
    int item_n, path_len;
    DIR_ITEM *items;
    DIR_ITEM *item_to_list;
    INODE *item_inode;
    INODE *cur_dir_inode;
    return_code result = CODE_OK;

    path_len = strlen(path);

    cur_dir_inode = calloc(sizeof(INODE), 1);
    item_inode = calloc(sizeof(INODE), 1);
    item_to_list = malloc(sizeof(DIR_ITEM));
    if (cur_dir_inode == NULL || item_inode == NULL) {
        return CODE_FATAL_ERROR;
    }


    if (path_len == 0) {
        fs_seek_set_i(global_current_dir->inode);
        fread(cur_dir_inode, sizeof(INODE), 1, global_filesystem);
    } else {
        result = utils_process_path(path, item_to_list);
        if (result == CODE_OK) {
            fs_seek_set_i(item_to_list->inode);
            fread(cur_dir_inode, sizeof(INODE), 1, global_filesystem);
        } else if (result == CODE_FATAL_ERROR) {
            return result;
        }
    }

    if (result == CODE_OK) {
        items = calloc(sizeof(DIR_ITEM), cur_dir_inode->references);

        if (items == NULL) {
            return CODE_FATAL_ERROR;
        }

        fs_seek_set_c(cur_dir_inode->direct1);
        fread(items, sizeof(DIR_ITEM), cur_dir_inode->references, global_filesystem);
        if (cur_dir_inode->isDirectory == true) {
            for (item_n = 0; item_n < cur_dir_inode->references; item_n++) {
                fs_seek_set_i(items[item_n].inode);
                fread(item_inode, sizeof(INODE), 1, global_filesystem);
                if (item_inode->isDirectory == true) {
                    printf("+ %d %s\n", item_inode->file_size, items[item_n].item_name);
                } else {
                    printf("- %d %s\n", item_inode->file_size, items[item_n].item_name);
                }
            }

        } else {
            result = CODE_WRONG_TYPE;
        }
        free(items);
    } else {
        result = CODE_PATH_NOT_FOUND;
    }

    free(item_to_list);

    free(item_inode);
    free(cur_dir_inode);
    return result;
}


/*
  Funkce změní aktuální adresář na adresář zadaný v předané cestě

   Proměnné:
            dir_path - zdrojová cesta k adresáři

   Vrací return_code:
                    CODE_OK              - Vše v pořádku
                    CODE_FATAL_ERROR     - Při operaci nastala chyba při práci s pamětí
                    CODE_PATH_NOT_FOUND  - Neexistuje cílová cesta
 */
return_code cd_func(char *dir_path) {
    DIR_ITEM *cur_dir_item = malloc(sizeof(DIR_ITEM));
    INODE *inode = malloc(sizeof(INODE));
    return_code result = CODE_OK;

    if (cur_dir_item == NULL || inode == NULL) {
        return CODE_FATAL_ERROR;
    }

    if(strlen(dir_path) == 0){ /* čistě cd = návrat do kořenového adresáře */
        utils_load_inode(inode, 1);
        result = utils_get_inode_item(inode, ".\0", cur_dir_item);
    }else{
        result = utils_process_path(dir_path, cur_dir_item);
    }


    if (result == CODE_OK) {
        utils_load_inode(inode, cur_dir_item->inode);

        if (inode->isDirectory == true) {
            if(cur_dir_item->inode != 1){
                if (strlen(cur_dir_item->item_name) == 2 && strncmp(cur_dir_item->item_name, "..", 2) == 0) {
                    utils_find_cur_dir_level_up(cur_dir_item);
                }else if(strlen(cur_dir_item->item_name) == 1 && strncmp(cur_dir_item->item_name, ".", 2) == 0){
                    utils_find_cur_dir_level_up(cur_dir_item);
                }
            }
            utils_copy_dir_item(cur_dir_item, global_current_dir);
        } else {
            result = CODE_WRONG_TYPE;
        }

    } else {
        if (result == CODE_FATAL_ERROR) {
            return result;
        } else {
            result = CODE_PATH_NOT_FOUND;
        }
    }

    free(cur_dir_item);
    free(inode);

    return result;
}


/*
  Funkce vypíše obsah souboru předaného v cestě jako parametr funkce.

   Proměnné:
            path - zdrojová cesta k adresáři

   Vrací return_code:
                    CODE_OK              - Vše v pořádku
                    CODE_FATAL_ERROR     - Při operaci nastala chyba při práci s pamětí
                    CODE_PATH_NOT_FOUND  - Neexistuje cílová cesta
 */
return_code cat_func(char *path) {
    DIR_ITEM *file;
    INODE *file_inode;
    return_code result = CODE_OK;
    int processed_data;
    int processed_clusters = 0;
    int byte_to_read = 0;
    char cur_data[CLUSTER_SIZE + 1];

    file = malloc(sizeof(DIR_ITEM));
    file_inode = malloc(sizeof(INODE));

    if (file == NULL || file_inode == NULL) {
        return CODE_FATAL_ERROR;
    }

    result = utils_process_path(path, file);

    if (result == CODE_FATAL_ERROR) {
        return result;
    }

    if (result == CODE_OK) {
        utils_load_inode(file_inode, file->inode);
        processed_data = file_inode->file_size;

        if (file_inode->isDirectory == false) {
            while (processed_data > 0) {
                if (processed_data >= CLUSTER_SIZE) {
                    byte_to_read = CLUSTER_SIZE;
                    processed_data -= CLUSTER_SIZE;
                } else {
                    byte_to_read = processed_data;
                    processed_data -= CLUSTER_SIZE;
                }

                utils_read_from_links(file_inode, processed_clusters, byte_to_read, cur_data);

                printf("%s", cur_data);
                processed_clusters++;
            }
        } else {
            result = CODE_WRONG_TYPE;
        }
    } else {
        result = CODE_PATH_NOT_FOUND;
    }

    free(file);
    free(file_inode);

    return result;
}


/*
  Funkce vypíše cestu aktuální adresáře

   Vrací return_code:
                    CODE_OK              - Vše v pořádku
                    CODE_FATAL_ERROR     - Při operaci nastala chyba při práci s pamětí
 */
return_code pwd_func() {
    int dir_level = 0, i;
    INODE *cur_inode;
    DIR_ITEM *cur_dir;
    DIR_ITEM *next_lev_dir;
    DIR_ITEM path[256];
    bool not_found = false;
    return_code result = CODE_OK;

    next_lev_dir = malloc(sizeof(DIR_ITEM));
    cur_dir = malloc(sizeof(DIR_ITEM));
    cur_inode = malloc(sizeof(INODE));

    if (cur_inode == NULL || cur_dir == NULL || next_lev_dir == NULL) {
        return CODE_FATAL_ERROR;
    }

    //přidání aktuálního adresáře
    utils_copy_dir_item(global_current_dir, cur_dir);
    path[dir_level++] = *cur_dir;

    if ((result = utils_find_dir_level_up(cur_dir, next_lev_dir)) == CODE_OK) {
        while (cur_dir->inode != next_lev_dir->inode) {
            path[dir_level++] = *next_lev_dir;
            utils_copy_dir_item(next_lev_dir, cur_dir);
            result = utils_find_dir_level_up(cur_dir, next_lev_dir);
            if (result == CODE_FATAL_ERROR) {
                return result;
            } else if (result == CODE_PATH_NOT_FOUND) {
                not_found = true;
                break;
            }
        }

        if (not_found == false) {
            if (dir_level == 1) {
                printf("/\n");
            } else {
                for (i = dir_level; i > 0; i--) {
                    if (i != dir_level) {
                        printf("/%s", path[i - 1].item_name);
                    }
                }
                printf("\n");
            }
        }

    }


    free(next_lev_dir);
    free(cur_dir);
    free(cur_inode);

    return result;
}


/*
  Funkce vypíše nápovědu k používaným funcím.
 */
void help_func() {
    printf("Simple i-node file system v1.0.\n\n");
    printf("Here is the list of supported functions\n\n");
    printf("cp <from> <to>\n");
    printf("rm <file>\n");
    printf("ls <path>\n");
    printf("cd <path>\n");
    printf("cat <path to file>\n");
    printf("pwd\n");
    printf("info <path to file>\n");
    printf("mkdir <path>\n");
    printf("rmdir <path>\n");
    printf("incp <source (out fs)> <destination (in fs)>\n");
    printf("outcp <source (in fs)> <destination (out fs)>\n");
    printf("check = checks filesystem files clusters\n");
    printf("load <source (out fs)>\n");
    printf("format <number (1-100)>K = Kilobytes | <number (1-100)>M = Megabytes \n");
}


/*
  Funkce vytvoří novou složku v předané cestě

   Proměnné:
            dir_location - zdrojová cesta k adresáři

   Vrací return_code:
                    CODE_OK               - Vše v pořádku
                    CODE_FATAL_ERROR      - Při operaci nastala chyba při práci s pamětí
                    CODE_PATH_NOT_FOUND   - Neexistuje cílová cesta
                    CODE_FS_OUT_OF_I      - Došli Inody aby se mohl vytvořit nový soubor
                    CODE_COM_WRONG_F_NAME - špatná délka jména souboru
                    CODE_FS_FULL_FS       - Do filesystému už nelze nic zapsat protože je plný
 */
return_code mkdir_func(char *dir_location) {
    int inode_id, needed_clusters, same_names, has_dir_space;
    DIR_ITEM *dir_dot, *dir_double_dot, *dir_item, *parent_dir;
    INODE *inode;
    int32_t *free_clusters;
    int32_t offset_dir_item;
    char dir_name[MAX_FILE_NAME];
    char dir_path_to_parent[MAX_PATH_LEN];
    return_code code = CODE_OK;

    utils_intersect_directory_path_and_file_name(dir_location, dir_path_to_parent, dir_name);

    parent_dir = malloc(sizeof(DIR_ITEM));

    if (parent_dir == NULL) {
        return CODE_FATAL_ERROR;
    }

    code = utils_process_path(dir_path_to_parent, parent_dir);

    if (code == CODE_FATAL_ERROR) {
        return code;
    }


    if (code == CODE_OK) {
        if (strlen(dir_name) > MAX_FILE_NAME - 1 || strlen(dir_name) < 1) {
            printf(COMMAND_FILE_NAME_WRONG_LEN);
            return CODE_COM_WRONG_F_NAME;
        } else if (utils_has_dir_space(parent_dir, &offset_dir_item, &has_dir_space) != CODE_OK) {
            return CODE_FATAL_ERROR;
        }

        if (has_dir_space == -1) {
            return CODE_FS_FULL_DIR;
        }

        /* Alokace paměti */
        inode = calloc(sizeof(INODE), 1);
        dir_dot = calloc(sizeof(DIR_ITEM), 1);
        dir_double_dot = calloc(sizeof(DIR_ITEM), 1);
        dir_item = calloc(sizeof(DIR_ITEM), 1);


        needed_clusters = 1; /* Předpoklad že soubory budou ukládány pouze do jednoho clusteru a použije se jen jedna reference u Inodu */
        free_clusters = malloc(sizeof(int32_t) * needed_clusters);

        if (inode == NULL || dir_dot == NULL || dir_double_dot == NULL || dir_item == NULL || free_clusters == NULL) {
            return CODE_FATAL_ERROR;
        }

        //zkontroluje jestli se v adresáři nenachází soubor se stejným jménem
        //pokud ne vrací 0 jinak -1
        if (utils_check_dir_items_names(parent_dir, dir_name, &same_names) != CODE_OK) {
            return CODE_FATAL_ERROR;
        }


        if (same_names == 0) {
            if (find_free_clusters(&free_clusters, needed_clusters) == 0) {
                /* Získání id pro inode */
                inode_id = utils_find_first_free_inode();
                if (inode_id != -1) {
                    inode->nodeid = inode_id;
                    inode->isDirectory = true;
                    inode->direct1 = free_clusters[0];
                    inode->file_size = CLUSTER_SIZE;
                    inode->references = 2;

                    dir_dot->inode = inode->nodeid;
                    strncpy(dir_dot->item_name, ".\0", 4);

                    dir_double_dot->inode = parent_dir->inode; //Odkaz na adresář do kterého se adresář vytváří
                    strncpy(dir_double_dot->item_name, "..\0", 4);

                    dir_item->inode = inode->nodeid;
                    strncpy(dir_item->item_name, dir_name, strlen(dir_name));


                    fs_seek_set_i(inode->nodeid);
                    fwrite(inode, sizeof(INODE), 1, global_filesystem); //uložení inodu

                    if (utils_write_file_reference_to_dir(parent_dir, dir_item, &offset_dir_item) == CODE_OK) {
                        fs_seek_set_c(inode->direct1);
                        fwrite(dir_dot, sizeof(DIR_ITEM), 1, global_filesystem);//fseek se automaticky posune;
                        fwrite(dir_double_dot, sizeof(DIR_ITEM), 1, global_filesystem);
                    } else {
                        code = CODE_FATAL_ERROR;
                    }
                } else {
                    code = CODE_FS_OUT_OF_I;
                }
            } else {
                return CODE_FS_FULL_FS;
            }
        } else {
            return CODE_FS_SAME_NAMES;
        }
    } else {
        return CODE_PATH_NOT_FOUND;
    }

    free(parent_dir);
    free(inode);
    free(dir_dot);
    free(dir_double_dot);
    free(dir_item);
    free(free_clusters);

    return code;
}


/*
  Funkce smaže adresář v předané cestě

   Proměnné:
            dir_location - zdrojová cesta k adresáři

   Vrací return_code:
                    CODE_OK               - Vše v pořádku
                    CODE_FATAL_ERROR      - Při operaci nastala chyba při práci s pamětí
 */
return_code rmdir_func(char *dir_location) {
    int has_match, is_dir_empty;
    INODE *dir_inode, *parrent_dir_inode;
    DIR_ITEM *dir_to_remove, *parent_dir;
    char dir_name[FILENAME_MAX];
    char dir_parent_path[MAX_PATH_LEN];

    return_code result;


    parent_dir = malloc(sizeof(DIR_ITEM));




    if (parent_dir == NULL) {
        return CODE_FATAL_ERROR;
    }
    utils_intersect_directory_path_and_file_name(dir_location, dir_parent_path, dir_name);
    result = utils_process_path(dir_parent_path, parent_dir);

    if (strlen(dir_name) == 2 && strncmp(dir_name, "..", 2) == 0) {
        printf("Forbiden action. Cant remove '..'\n");
    }else if(strlen(dir_name) == 1 && strncmp(dir_name, ".", 2) == 0){
        printf("Forbiden action. Cant remove '.'\n");
    }else{
        if (result == CODE_FATAL_ERROR) {
            return result;
        }


        if (result == CODE_OK) {
            if (utils_check_dir_items_names(parent_dir, dir_name, &has_match) != CODE_OK) {
                return CODE_FATAL_ERROR;
            }
            if (has_match == -1) {
                dir_to_remove = malloc(sizeof(DIR_ITEM));
                dir_inode = calloc(sizeof(INODE), 1);
                parrent_dir_inode = calloc(sizeof(INODE), 1);

                if ((result = utils_get_dir_item(parent_dir, dir_name, dir_to_remove)) != CODE_OK) {
                    return result;
                }

                if(result == CODE_OK){
                    if (dir_inode == NULL
                        || parrent_dir_inode == NULL
                        || parrent_dir_inode == NULL
                        || utils_is_directory_empty(dir_to_remove, &is_dir_empty) != CODE_OK) {
                        return CODE_FATAL_ERROR;
                    }

                    utils_load_inode(dir_inode, dir_to_remove->inode);

                    if(dir_inode->isDirectory == true){
                        if (is_dir_empty == 0) {
                            //Přečtení inodu adresáře ve kterém se nachází adresář který se má smazat
                            fs_seek_set_i(parent_dir->inode);
                            fread(parrent_dir_inode, sizeof(INODE), 1, global_filesystem);

                            utils_remove_dir_reference(dir_inode, parrent_dir_inode,dir_name, true);
                        } else {
                            printf(COMMAND_NOT_EMPTY);
                            return CODE_OK;
                        }
                    }else{
                        result = CODE_WRONG_TYPE;
                    }
                }else{
                    result = CODE_FILE_NOT_FOUND;
                }

                free(parrent_dir_inode);
                free(dir_inode);
                free(dir_to_remove);
            } else {
                result = CODE_FILE_NOT_FOUND;
            }
        } else {
            result = CODE_FILE_NOT_FOUND;
        }
    }



    free(parent_dir);
    return result;
}


/*
  Funkce vypíše informace o inodu file itemu na zadané cestě

   Proměnné:
            dir_location - cesta k file itemu

   Vrací return_code:
                    CODE_OK               - Vše v pořádku
                    CODE_FATAL_ERROR      - Při operaci nastala chyba při práci s pamětí
 */
return_code info_func(char *path) {
    DIR_ITEM *file = malloc(sizeof(DIR_ITEM));
    return_code result = CODE_OK;

    if (file == NULL) {
        return CODE_FATAL_ERROR;
    }
    result = utils_process_path(path, file);
    if (result == CODE_OK) {
        utils_print_inode_info(file->inode);
    }

    free(file);
    return result;

}

/*
  Funkce nahraje soubor ze filesystému počítače a provede všechny funce mého filesystému, které tam jsou zapsané

   Proměnné:
            path - cesta ve filesystému počítače ze které se bude souboru nahrávat

   Vrací return_code:
                        CODE_OK               - Příkaz byl vykonán v pořádku
                        CODE_MISSING_ARGUMENT - Chybí argument v příkazu
                        CODE_FATAL_ERROR      - Došlo k závažné chybě pří práci v paměti
                        CODE_FS_LOAD_FAILURE  - Nepodařilo se načíst filesystém
                        CODE_FS_ERROR         - Chyba ve fylesystému
                        CODE_FS_FULL_FS       - Fylesystém je plný
                        CODE_FS_FULL_DIR      - Adresář je plný
                        CODE_FS_OUT_OF_I      - Všechny Inody jsou zabrané
                        CODE_FS_SAME_NAMES    - Stejné jména filů
                        CODE_COM_WRONG_F_NAME - Neakceptovatelné jméno souboru
                        CODE_PATH_NOT_FOUND   - Neexistující cesta
                        CODE_FILE_NOT_FOUND   - Neexistující soubor
                        CODE_NOT_EMPTY        - adresář není prázdný
                        CODE_EXIT_CODE        - signál pro ukončení filesystému
 */
return_code load_func(char *path) {
    FILE *file;
    file = fopen(path, "r");
    char line[COMMAND_BUFF_SIZE];
    return_code result = CODE_OK;

    if (file != NULL) {
        while (fgets(line, COMMAND_BUFF_SIZE, file)) {
            printf("MyFS>%s", line);
            result = execute_command(line);

            if(result == CODE_FS_FULL_DIR){
                printf(COMMAND_DIR_FULL);
            }else if(result == CODE_PATH_NOT_FOUND){
                printf(COMMAND_PATH_NOT_FOUND);
            }else if(result == CODE_FILE_NOT_FOUND){
                printf(COMMAND_FILE_NOT_FOUND);
            }else if(result == CODE_FS_FULL_FS){
                printf(COMMAND_FILESYSTEM_FULL);
            }else if(result == CODE_FS_SAME_NAMES){
                printf(COMMAND_EXISTS);
            }else if(result == CODE_NOT_EMPTY) {
                printf(COMMAND_NOT_EMPTY);
            }else if(result == CODE_COM_WRONG_F_NAME) {
                printf(COMMAND_WRONG_NAME);
            }else if (result == CODE_WRONG_TYPE){
                printf(COMMAND_WRONG_TYPE);
            }else if(result == CODE_EXIT_CODE){
                result = CODE_OK;
                break;
            }else if(result != CODE_OK){
                break;
            }

        }
        fclose(file);
    } else {
        return CODE_FILE_NOT_FOUND;
    }

    return result;

}


/*
  Funkce nahraje item z filesystému počítače do mého filesystému.

   Proměnné:
            source - cesta ve filesystému počítače ze které se bude souboru nahrávat
            dest   - umístění nově vytvořeného souboru

   Vrací return_code:
                    CODE_OK               - Vše v pořádku
                    CODE_FATAL_ERROR      - Při operaci nastala chyba při práci s pamětí
                    CODE_PATH_NOT_FOUND   - Neexistuje cílová cesta
                    CODE_FILE_NOT_FOUND   - Neexistuje zdrojová cesta
                    CODE_FS_OUT_OF_I      - Došli Inody aby se mohl vytvořit nový soubor
                    CODE_COM_WRONG_F_NAME - špatná délka jména souboru
                    CODE_FS_FULL_FS       - Do filesystému už nelze nic zapsat protože je plný
 */
return_code incp_func(char *source, char *dest) {
    FILE *fp;
    INODE *inode;
    DIR_ITEM *file_item;
    DIR_ITEM *file_directory;
    int32_t inode_id;
    int32_t file_offset;
    int file_size = 0;
    int has_dir_space = 0;
    int processed_cluster = 0;
    int bytes_to_write = 0;
    int processed_data = 0;
    int number_of_needed_cluster = 0;
    int exist_same_name_file_in_dir = 0;
    char data_to_write[CLUSTER_SIZE + 1];
    char dir_path[MAX_PATH_LEN];
    char file_name[MAX_FILE_NAME];
    int32_t *free_clusters;
    return_code returnCode = CODE_OK;


    fp = fopen(source, "r");

    if (fp != NULL) {
        fseek(fp, 0L, SEEK_END);
        file_size = ftell(fp);
        rewind(fp);
        number_of_needed_cluster = utils_calc_needed_cluster_for_file(file_size);
        file_directory = malloc(sizeof(DIR_ITEM));
        if (file_directory != NULL) {

        }
        //vytvoření DIR_ITEM
        utils_intersect_directory_path_and_file_name(dest, dir_path, file_name);
        returnCode = utils_process_path(dir_path, file_directory);
        if (returnCode == CODE_FATAL_ERROR) {
            return returnCode;
        }

        /* Neexistuje cesta do cílové složky */
        if (returnCode == CODE_OK) {
            if (utils_has_dir_space(file_directory, &file_offset, &has_dir_space) != CODE_OK) {
                return CODE_FATAL_ERROR;
            }
            /* Kontrola že adresář neobsahuje soubor se stejným jménem */
            if ((returnCode = utils_check_dir_items_names(file_directory, file_name, &exist_same_name_file_in_dir)) ==
                CODE_FATAL_ERROR) {
                return returnCode;
            }
            if (strlen(file_name) < MAX_FILE_NAME && strlen(file_name) > 0) {
                if (exist_same_name_file_in_dir == 0) {
                    if (has_dir_space == 0) {
                        //Ukládání dat vůči inodu
                        free_clusters = malloc(number_of_needed_cluster * sizeof(int32_t));
                        inode = malloc(sizeof(INODE));
                        file_item = malloc(sizeof(DIR_ITEM));
                        inode_id = utils_find_first_free_inode();
                        if (free_clusters == NULL || inode == NULL || file_item == NULL) {
                            return CODE_FATAL_ERROR;
                        } else if (inode_id == -1) {
                            returnCode = CODE_FS_OUT_OF_I;
                        } else {
                            if (find_free_clusters(&free_clusters, number_of_needed_cluster) == 0) {
                                inode->nodeid = inode_id;
                                inode->file_size = file_size;
                                inode->isDirectory = false;
                                inode->references = 0;
                                utils_assign_clusters_to_inode(inode, free_clusters, number_of_needed_cluster);
                                fs_seek_set_i(inode_id);
                                fwrite(inode, sizeof(INODE), 1, global_filesystem);

                                processed_data = file_size;
                                while (processed_data > 0) {
                                    memset(data_to_write, '\0', CLUSTER_SIZE + 1);
                                    if (processed_data >= CLUSTER_SIZE) {
                                        bytes_to_write = CLUSTER_SIZE;
                                    } else {
                                        bytes_to_write = processed_data;
                                    }
                                    fread(data_to_write, sizeof(char), bytes_to_write, fp);
                                    processed_data -= CLUSTER_SIZE;

                                    utils_write_byte_to_links(inode, data_to_write, processed_cluster, bytes_to_write);
                                    processed_cluster++;
                                }

                                file_item->inode = inode_id;
                                memset(file_item->item_name, '\0', MAX_FILE_NAME);
                                strncpy(file_item->item_name, file_name, MAX_FILE_NAME);
                                utils_write_file_reference_to_dir(file_directory, file_item, &file_offset);
                            } else {
                                returnCode = CODE_FS_FULL_FS;
                            }
                        }

                        fclose(fp);
                        free(file_item);
                        free(inode);
                        free(free_clusters);
                    } else {
                        returnCode = CODE_FS_FULL_DIR;
                    }
                } else {
                    returnCode = CODE_FS_SAME_NAMES;
                }
            } else {
                returnCode = CODE_COM_WRONG_F_NAME;
            }
            free(file_directory);
        } else {
            return CODE_PATH_NOT_FOUND;
        }

    } else {
        return CODE_FILE_NOT_FOUND;
    }


    return returnCode;
}


/*
  Funkce nahraje item z mého filesystému do filesystému počítače.

   Proměnné:
            local_file - soubor ve mém filesytému
            new_file   - soubor ve filesystému počítače
   Vrací return_code:
                    CODE_OK               - Vše v pořádku
                    CODE_FATAL_ERROR      - Při operaci nastala chyba při práci s pamětí
                    CODE_PATH_NOT_FOUND   - Neexistuje cílová cesta
                    CODE_FILE_NOT_FOUND   - Neexistuje zdrojová cesta
 */
return_code outcp_funf(char *local_file, char *new_file) {
    FILE *fp;
    DIR_ITEM *file;
    INODE *file_inode;
    return_code result = CODE_OK;
    int processed_data;
    int processed_clusters = 0;
    int byte_to_read = 0;
    char cur_data[CLUSTER_SIZE + 1];

    file = malloc(sizeof(DIR_ITEM));
    file_inode = malloc(sizeof(INODE));

    if (file == NULL || file_inode == NULL) {
        return CODE_FATAL_ERROR;
    }

    result = utils_process_path(local_file, file);

    if (result == CODE_FATAL_ERROR) {
        return result;
    }

    if (result == CODE_OK) {
        fp = fopen(new_file, "w");
        if (fp != NULL) {
            utils_load_inode(file_inode, file->inode);
            processed_data = file_inode->file_size;

            while (processed_data > 0) {
                if (processed_data >= CLUSTER_SIZE) {
                    byte_to_read = CLUSTER_SIZE;
                    processed_data -= CLUSTER_SIZE;
                }else{
                    byte_to_read = processed_data;
                    processed_data -= CLUSTER_SIZE;
                }

                utils_read_from_links(file_inode, processed_clusters, byte_to_read, cur_data);

                fwrite(cur_data, sizeof(char), byte_to_read, fp);
                processed_clusters++;
            }
            fclose(fp);
        } else {
            result = CODE_PATH_NOT_FOUND;
        }
    }

    free(file);
    free(file_inode);

    return result;
}

/*
 * Jednoduchá kontrola filesystému. Neudělá nic jiného než že zkontroluje zda má každý Inode dostatečný počet přiřazených
 * clusterů vůči velikosti jakou uchovává v datech.
 *
 */
return_code check_func() {
    return utils_check_inodes_consistency();
}


/*
  Funkce nevratně poškodí inode, kdy se přemaže první přímý odkaz.

   Proměnné:
            fs_size - cesta k file který se má poškodit

   Vrací return_code:
                    CODE_OK              - Poškození se povedlo
                    CODE_FILE_NOT_FOUND  - Soubor nebyl nalezen
                    CODE_FATAL_ERROR     - Chyba v paměti
 */
return_code corrupt_func(char *path){
    DIR_ITEM *file_item;
    INODE *file_inode;
    return_code result;

    file_inode = malloc(sizeof(INODE));
    file_item = malloc(sizeof (INODE));

    result = utils_process_path(path, file_item);

    if(result == CODE_FATAL_ERROR){
        return result;
    }

    if(result == CODE_OK){
        utils_load_inode(file_inode, file_item->inode);
        file_inode->direct1 = -1;

        fs_seek_set_i(file_inode->nodeid);
        fwrite(file_inode, sizeof(INODE), 1, global_filesystem);
    }else{
        result = CODE_FILE_NOT_FOUND;
    }

    free(file_item);
    free(file_inode);

    return result;
}

/*
  Funkce vypíše informace o inodu podle předaného id inodu.

   Proměnné:
            inode_id - id inodu
 */
void iinfo_func(char *inode_id) {
    int id = atoi(inode_id);
    utils_print_inode_info(id);
}

/*
   Funkce vypíše informace ze superblocku
 */
void sbinfo_func() {
    utils_print_sb_info();
}


/*
  Funkce naformátuje filesystém na zadanou velikost

   Proměnné:
            fs_size - velikost na kterou se naformátuje filesystém

   Vrací return_code:
                    CODE_OK              - Soubor byl v inodu nalezen
                    CODE_FS_LOAD_FAILURE - Chyba při formátování filesystému
                    CODE_FS_ERROR        - Chyba při práci s filesystémem
 */
return_code format_func(char *fs_size) {
    return filesystem_format_fs(fs_size);
}