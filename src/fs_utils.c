#include <memory.h>
#include <unistd.h>
#include <string.h>
#include <math.h>
#include "fs_utils.h"
#include "filesystem_cache.h"
#include "filesystem.h"


/*
   Pomocná funkce, která připravý byte, který se skládá ze 7 jedniček
   a jedné nuly, která se nastaví na pozice z prava podle předaného parametru.

   Proměnné:
            id - pozice v bytu kde nebude 1

   Vrací uint8_t ze 7 jedniček a jedné 0 nuly umístěné podle parametru
 */
uint8_t prepare_modified_byte(int id) {
    int i;
    uint8_t res = 0x00;
    uint8_t modified_bit = 0x80; /* 1000 0000 */

    for (i = 0; i < 8; i++) {

        if (i == id) {
            continue;
        } else {
            res = res | (modified_bit >> i);
        }
    }

    return res;
}


/*
   Metoda vypočte kolik je potřeba bytů pro bitmapu na základě požadavku v bitech.
   Napr. při požadku 13 bitů vrátí 2 (znamená 2 byty)

   Proměnné:
            req_bits - počet požadovaných bitů pro bitmapu

   Vrací int32_t. Hodnota představuje počet bytů potřebných pro bitmapu.
 */
int32_t calc_bytes_in_bitmap(int req_bits) {
    int bytes;

    if (req_bits == 0) {
        return -1;
    }

    if (req_bits % 8 != 0) {
        bytes = (req_bits / 8) + 1; /* zaokrouhlení na 8 bitů */
    } else {
        bytes = req_bits / 8;
    }

    return bytes;
}

/*
   Vypočítá kolik bude i nodů ve filesystému na základě určeného poměru

   Proměnné:
            fs_size    - Velikost filesystému

   Vrací int32_t. Hodnota představuje počet volných inodů.
 */
int32_t calc_number_of_inodes(int32_t fs_size) {
    int memory_for_inodes = fs_size * INODE_PERC;
    int i_nodes_number = 0;

    i_nodes_number = memory_for_inodes / sizeof(INODE);


    return i_nodes_number;

}


/*
   Metoda vypočítá počet datových clusterů. Jedná se o místo který zbyte po zabrání superblocku a inodů

   Proměnné:
            sb_size    - Velikost superblocku
            inodes_num - počet inodů
            fs_size    - Velikost filesystému

   Vrací int32_t. Hodnota představuje počet volných datových clusterů.
 */
int32_t calc_number_of_data_cluster(int sb_size, int inodes_num, int fs_size) {
    int32_t used = sb_size + inodes_num * sizeof(INODE);
    int32_t free = fs_size - used;
    int32_t clust_num = free / CLUSTER_SIZE;
    int32_t clust_af_cor = clust_num;
    int32_t free_with_clusters = free - (clust_num * CLUSTER_SIZE);
    int32_t bytes_for_bitmap = 0;

    bytes_for_bitmap = calc_bytes_in_bitmap(clust_num);


    /* Přidá místo pro bitmapu clusterů */
    while (free_with_clusters < bytes_for_bitmap) {
        free_with_clusters += CLUSTER_SIZE;
        clust_af_cor--;
    }

    return clust_af_cor;

}

/*
   Metoda nastaví seek na zadanou hodnotu offsetu.

   Proměnné:
            offset - pozice kam se má nastavit seek.
 */
void fs_seek_set(int32_t offset) {
    if (offset > INT32_MAX) {
        offset -= INT32_MAX;
        fseek(global_filesystem, INT32_MAX, SEEK_SET);
        fseek(global_filesystem, offset, SEEK_CUR);
    } else {
        fseek(global_filesystem, offset, SEEK_SET);
    }
}


/*
   Metoda nastaví seek na začátek inodu zadané skrz předané id.

   Proměnné:
            inode_id - id inodu
 */
void fs_seek_set_i(int32_t inode_id) {
    int32_t offset;
    offset = global_sb->inode_start_address + (sizeof(INODE) * (inode_id - 1)); /* číslování inodu od 1 */
    if (inode_id > INT32_MAX) {
        offset -= INT32_MAX;
        fseek(global_filesystem, INT32_MAX, SEEK_SET);
        fseek(global_filesystem, offset, SEEK_CUR);
    } else {
        fseek(global_filesystem, offset, SEEK_SET);
    }
}


/*
   Nastaví seek na cluster se zadaným id.

   Proměnné:
            cluster_id   - id clusteru na který se má nastavit seek
 */
void fs_seek_set_c(int32_t cluster_id) {
    int32_t offset = 0;
    offset = global_sb->data_start_address + (CLUSTER_SIZE * cluster_id);
    if (cluster_id > INT32_MAX) {
        offset -= INT32_MAX;
        fseek(global_filesystem, INT32_MAX, SEEK_SET);
        fseek(global_filesystem, offset, SEEK_CUR);
    } else {
        fseek(global_filesystem, offset, SEEK_SET);
    }
}


/*
   Nastaví seek na N-tý cluster od 0 na první indirect link

   Proměnné:
            indirect_id  - nepřímý odkaz prvního řádu
            cluster_id   - id clusteru na který se má nastavit seek
            print        - proměná jestli se má tisknout info o odkazech. true ano, false ne.
            inref        - proměná jestli se má tisknout info o nepřímých odkazech. true ano, false ne.

   Vrací int. Id nepřímého odkazu na cluster.
 */
int32_t fs_seek_set_in1_c(int32_t indirect_id, int32_t cluster_id, bool print, bool inref) {
    int32_t cluster;
    /* offset indirect clusteru */
    int32_t offset = global_sb->data_start_address + (CLUSTER_SIZE * indirect_id);
    /* offset ukazující na konkretní direct link v indirect clusteru */
    offset += sizeof(int32_t) * cluster_id;
    fseek(global_filesystem, offset, SEEK_SET);
    /* přečtení id clusteru */
    fread(&cluster, sizeof(int32_t), 1, global_filesystem);
    /* nastavení seeku na cluster */
    fs_seek_set_c(cluster);

    if (print == true && inref == false) {
        printf("    ref-%d: %d\n", cluster_id + 1, cluster);
    } else if (print == true && inref == true) {
        printf("    inref-%d:\n", cluster);
    }
    return cluster;
}


/*
   Motoda nastaví seek na cluster s id cluster_id v nepřímém odkazu druhého řádu

   Proměnné:
            indirect_id      - nepřímý odkaz prvního řádu
            second_indirect  - nepřímý odkaz druhého řádu
            cluster_id       - id clusteru
            print            - určuje jestli se bude tisknout info o druhém nepřímém odkazu. true ano, false ne.
            print_first      - určuje jestli se bude tisknout info o prvním nepříměm odkazu. true ano, false ne.
 */
int32_t fs_seek_set_in2_c(int32_t indirect_id, int32_t second_indirect, int32_t cluster_id, bool print, bool print_first) {
    int32_t offset;
    int32_t cluster;
    fs_seek_set_in1_c(indirect_id, second_indirect, print_first, true);
    offset = ftell(global_filesystem);
    fseek(global_filesystem, offset + sizeof(int32_t) * cluster_id, SEEK_SET);
    fread(&cluster, sizeof(int32_t), 1, global_filesystem);

    fs_seek_set_c(cluster);

    if (print == true) {
        printf("        ref-%d: %d\n", cluster_id + 1, cluster);
    }
    return cluster;
}


/*
   Metoda načte out_inode z disku na základě předaného id inodu.

   Proměnné:
            out_inode              - proměná do které se načte out_inode
            node_id            - id_inodu, který se má načíst
 */
void utils_load_inode(INODE *out_inode, int node_id) {
    fs_seek_set_i(node_id);
    fread(out_inode, sizeof(INODE), 1, global_filesystem);
}


/*
   Upraví bit v bitmapě, jestli je použitý nebo naopak pro uvolnění. Bitmapa je číslovaná od 0.

   Proměnné:
            bitmap_id              - pozice bitu v bitmapě
            used                   - pokud je true nastavuje se v bitmapě bit na to že je použitý, naopak že je volný
            count_of_bits          - počet bitů, který se reálně v bitmapě používají
            start_of_bitmap        - adresa začátku bitmapy ve filesystému

   Vrací return_code:
                    CODE_OK              - Soubor byl v inodu nalezen
                    CODE_FS_ERROR        - Snaha zapsat na již použitý bit
 */
return_code utils_modify_bitmap(int bitmap_id, bool used, int32_t count_of_bits, int32_t start_of_bitmap) {
    int bitmap_part_num, bitmap_parts;
    bitmap_parts = calc_bytes_in_bitmap(count_of_bits);
    uint8_t bitmap[bitmap_parts];
    uint8_t part_of_bitmap;
    uint8_t modified_bit = 0x80; /* 1000 0000 - to se bude shiftovat podle toho jaký bit v bitmapě se má použít */

    fs_seek_set(start_of_bitmap);
    fread(bitmap, sizeof(uint8_t), bitmap_parts, global_filesystem);

    bitmap_part_num = bitmap_id / 8;
    part_of_bitmap = bitmap[bitmap_part_num];

    /* kontrola že je volný */

    part_of_bitmap = part_of_bitmap << (bitmap_id % 8);
    part_of_bitmap = part_of_bitmap & 0x01;

    if (used == true) {
        if (part_of_bitmap == 1) {
            return CODE_FS_ERROR;
        } else {
            part_of_bitmap = bitmap[bitmap_part_num];
            modified_bit = modified_bit >> (bitmap_id % 8);
            part_of_bitmap = part_of_bitmap | modified_bit;
        }
    } else {
        part_of_bitmap = bitmap[bitmap_part_num];
        modified_bit = prepare_modified_byte(bitmap_id %
                                             8); /* Připraví se modified byte pro logický and, tak aby se nastavila nula jen na požadovanou pozici */
        part_of_bitmap = part_of_bitmap & modified_bit;
    }

    bitmap[bitmap_part_num] = part_of_bitmap;

    fs_seek_set(start_of_bitmap);
    fwrite(bitmap, sizeof(uint8_t), bitmap_parts, global_filesystem);

    return CODE_OK;
}


/*
  Metoda označí cluster v bitmapě jako volný, což znamená že je cluter i zároveň uvolněn a může být přepsán novými daty.

  Proměnné:
           cluster_id - id clusteru který se má uvolnit
*/
void utils_free_cluster(int cluster_id) {
    utils_modify_bitmap(cluster_id, false, (*global_sb).cluster_count, (*global_sb).bitmap_start_address);
}


/*
  Metoda označí inode v bitmapě jako volný, což znamená že je inode i zároveň uvolněn a může být přepsán novými daty.

  Proměnné:
           inode_id - id inodu který se má uvolnit
*/
void utils_free_inode(int inode_id) {
    /* inody jsou číslované od 1 ale pole jede od 0 */
    utils_modify_bitmap(inode_id - 1, false, (*global_sb).inode_count, (*global_sb).bitmapi_start_address);
}


/*
   Vyhledá v bitmapě inodu první volný inode a ten vrátí. V případě, že žádný volný není,
   tak vrací -1. Zároveň vrácený inode označí jako použitý v bitmapě inodů.

   Vrací int. Číselná hodnota znamená id inodu který je volný. V případě že žádný volný není je vráceno -1.
 */
int utils_find_first_free_inode() {
    int inode_id, bitmap_part_num, bitmap_parts;
    bitmap_parts = calc_bytes_in_bitmap((*global_sb).inode_count);
    uint8_t inode_bitmap[bitmap_parts];
    uint8_t bitmap_part;

    fs_seek_set((*global_sb).bitmapi_start_address);
    fread(inode_bitmap, sizeof(uint8_t), bitmap_parts, global_filesystem);

    for (inode_id = 0; inode_id < (*global_sb).inode_count; inode_id++) {
        bitmap_part_num = inode_id / 8;
        bitmap_part = inode_bitmap[bitmap_part_num];
        bitmap_part = bitmap_part << (inode_id % 8);
        bitmap_part = bitmap_part & 0x80;

        if (bitmap_part == 0) {
            utils_modify_bitmap(inode_id, true, (*global_sb).inode_count, (*global_sb).bitmapi_start_address);
            break;
        } else if (inode_id == (*global_sb).inode_count - 1) {
            return -1; /* žádný volný inode není */
        }
    }

    return inode_id + 1;
}

/*
   Vypočítá kolik je volných clusterů

   Vrací int představující počet volných clusterů.
 */
int calc_number_of_free_clusters() {
    int cluster_id, bitmap_part_num, bitmap_parts;
    bitmap_parts = calc_bytes_in_bitmap((*global_sb).cluster_count);
    uint8_t cluster_bitmap[bitmap_parts];
    uint8_t bitmap_part = 0;
    int free_clu_num = 0;

    fs_seek_set((*global_sb).bitmap_start_address);
    fread(cluster_bitmap, sizeof(uint8_t), bitmap_parts, global_filesystem);

    for (cluster_id = 0; cluster_id < (*global_sb).cluster_count; cluster_id++) {
        bitmap_part_num = cluster_id / 8;
        bitmap_part = cluster_bitmap[bitmap_part_num];
        bitmap_part = bitmap_part >> (cluster_id % 8);
        bitmap_part = bitmap_part & 0x01;

        if (bitmap_part == 0) {
            ++free_clu_num;
        }
    }

    return free_clu_num;
}

/*
   Vypočítá kolik je volných inodů

   Vrací int představující počet volných inodů.
 */
int calc_number_of_free_inodes() {
    int inode_id, bitmap_part_num, bitmap_parts;
    bitmap_parts = calc_bytes_in_bitmap((*global_sb).inode_count);
    uint8_t cluster_bitmap[bitmap_parts];
    uint8_t bitmap_part = 0;
    int free_clu_num = 0;

    fs_seek_set((*global_sb).bitmapi_start_address);
    fread(cluster_bitmap, sizeof(uint8_t), bitmap_parts, global_filesystem);

    for (inode_id = 0; inode_id < (*global_sb).inode_count; inode_id++) {
        bitmap_part_num = inode_id / 8;
        bitmap_part = cluster_bitmap[bitmap_part_num];
        bitmap_part = bitmap_part >> (inode_id % 8);
        bitmap_part = bitmap_part & 0x01;

        if (bitmap_part == 0) {
            ++free_clu_num;
        }
    }

    return free_clu_num;
}

/*
   Najde všechny použité inody a vratí je skrz pointer

     Proměnné:
              out_used_inodes - vrací pole id_inodu, které jsou použité
 */
void find_used_inodes(int *out_used_inodes) {
    int inode_id, bitmap_part_num, bitmap_parts;
    bitmap_parts = calc_bytes_in_bitmap((*global_sb).inode_count);
    uint8_t cluster_bitmap[bitmap_parts];
    uint8_t bitmap_part = 0;
    int free_clu_num = 0;

    fs_seek_set((*global_sb).bitmapi_start_address);
    fread(cluster_bitmap, sizeof(uint8_t), bitmap_parts, global_filesystem);

    for (inode_id = 0; inode_id < (*global_sb).inode_count; inode_id++) {
        bitmap_part_num = inode_id / 8;
        bitmap_part = cluster_bitmap[bitmap_part_num];
        bitmap_part = bitmap_part << (inode_id % 8);
        bitmap_part = bitmap_part & 0x80;

        if (bitmap_part == 128) {
            out_used_inodes[free_clu_num++] = inode_id + 1; /* číslování inodu od 1 */
        }
    }
}

/*
   Najde všechny použité inody a vratí je skrz pointer

     Proměnné:
              out_used_cluster - vrací pole id_inodu, které jsou použité
 */
void find_used_clusters(int *out_used_cluster) {
    int inode_id, bitmap_part_num, bitmap_parts;
    bitmap_parts = calc_bytes_in_bitmap((*global_sb).cluster_count);
    uint8_t cluster_bitmap[bitmap_parts];
    uint8_t bitmap_part = 0;
    int cluster_id = 0;

    fs_seek_set((*global_sb).bitmap_start_address);
    fread(cluster_bitmap, sizeof(uint8_t), bitmap_parts, global_filesystem);

    for (inode_id = 0; inode_id < (*global_sb).cluster_count; inode_id++) {
        bitmap_part_num = inode_id / 8;
        bitmap_part = cluster_bitmap[bitmap_part_num];
        bitmap_part = bitmap_part << (inode_id % 8);
        bitmap_part = bitmap_part & 0x80;

        if (bitmap_part == 128) {
            out_used_cluster[cluster_id++]++;
        }else{
            cluster_id++;
        }

    }
}

/*
  Prohledá bitmapu clusterů a spočítá kolik je volných.
  Pokud je volných dostatek, podle předaného parametru požadovaných clusterů,
  tak vrátí vektor volných clusterů.

  Proměnné:
            out_free_clusters         - přes pointer vrátí vektor volných clusterů. resp. jejich id
            number_of_needed_clusters - počet požadovaných clusterů

   Vrací int. 0 pokud se vše povedlo nebo -1 v případě že není dostatek volných clusterů
 */
int find_free_clusters(int32_t **out_free_clusters, int number_of_needed_clusters) {
    int cluster_id, bitmap_part_num, bitmap_parts, free_clusters;
    int free_index = 0;
    bitmap_parts = calc_bytes_in_bitmap((*global_sb).cluster_count);
    free_clusters = calc_number_of_free_clusters();
    int32_t *free_cluster_arr;
    uint8_t cluster_bitmap[bitmap_parts];
    uint8_t bitmap_part;

    free_cluster_arr = *out_free_clusters;

    if (free_clusters < 1 || number_of_needed_clusters > free_clusters) {
        return -1;
    }

    fs_seek_set((*global_sb).bitmap_start_address);
    fread(cluster_bitmap, sizeof(uint8_t), bitmap_parts, global_filesystem);

    for (cluster_id = 0; cluster_id < (*global_sb).cluster_count; cluster_id++) {
        bitmap_part_num = cluster_id / 8;
        bitmap_part = cluster_bitmap[bitmap_part_num];
        bitmap_part = bitmap_part << (cluster_id % 8);
        bitmap_part = bitmap_part & 0x80;

        if (bitmap_part == 0) {
            free_cluster_arr[free_index] = cluster_id;
            utils_modify_bitmap(cluster_id, true, (*global_sb).cluster_count, (*global_sb).bitmap_start_address);
            ++free_index;
            if (free_index == number_of_needed_clusters) {
                break;
            }
        }
    }
    *out_free_clusters = free_cluster_arr;

    return 0;
}


/*
   Spočítá kolik je potřeba clusterů pro soubor včetně indirect linků.

   Proměnné:
            size - velikost jaká je potřeba pro file.

   Vrací int který říká kolik je potřeba clusterů pro file včetně clusterů pro nepřímé odkazy.
 */

int utils_calc_needed_cluster_for_file(int size) {
    int needed_clusters = 0;
    int temp_res = 0;
    int extra_cluster_for_indirect = 0;

    needed_clusters = (int) ceil((double) size / CLUSTER_SIZE);
    temp_res = size - (5 * CLUSTER_SIZE);/* 5 = počet direct linků */

    if (temp_res > 0) {
        extra_cluster_for_indirect++;
        temp_res = temp_res - (CLUSTER_SIZE / sizeof(int32_t) * CLUSTER_SIZE);

        if (temp_res > 0) {
            extra_cluster_for_indirect++;
            extra_cluster_for_indirect += (int) ceil(
                    (double) temp_res / (CLUSTER_SIZE / sizeof(int32_t) * CLUSTER_SIZE));
        }
    }

    return needed_clusters + extra_cluster_for_indirect;
}


/*
  Zkontroluje že se v adresáři nenalézá soubor se stejným jménem.
  Vrací 0 pokud není soubor se stejným a -1 pokud je soubor se stejným jménem v adresáři.
  Ten vrátí přes pointer out_items_in_directory.

   Proměnné:
            directory              - directory ve které se zkoumá jestli neobsahuje soubor se stejným názvem (case sensitive)
            name                   - jméno souboru které se kontroluje jestli už není v directory
            out_items_in_directory - pointer přes který se vrací jestli obsahuje stejné soubory. 0 neobsahuje soubor
                                     se stejným jménem. -1 soubor se stejným jménem je v adresáři.

   Vrací return_code:
                    CODE_OK              - Soubor byl v inodu nalezen
                    CODE_FATAL_ERROR     - Při operaci nastala chyba při práci s pamětí
 */
return_code utils_check_dir_items_names(DIR_ITEM *directory, char *name, int *out_items_in_directory) {
    int i;
    DIR_ITEM *items_in_directory;
    INODE dir_inode;
    bool has_match = false;

    fs_seek_set_i(directory->inode);
    fread(&dir_inode, sizeof(INODE), 1, global_filesystem);

    items_in_directory = malloc(sizeof(DIR_ITEM) * dir_inode.references);

    if (items_in_directory == NULL) {
        return CODE_FATAL_ERROR;
    }

    fs_seek_set_c(dir_inode.direct1);
    fread(items_in_directory, sizeof(DIR_ITEM), dir_inode.references, global_filesystem);

    for (i = 0; i < dir_inode.references; i++) {
        if (strlen(items_in_directory[i].item_name) == strlen(name)) {
            if (strncmp(items_in_directory[i].item_name, name, strlen(name)) == 0) {
                has_match = true;
            }
        }
    }

    free(items_in_directory);
    if (has_match == true) {
        *out_items_in_directory = -1;
    } else {
        *out_items_in_directory = 0;
    }

    return CODE_OK;
}

/*
   V předaném inodu hledá jestli obsahuje soubor o zadaném jméně.
   Pokud ano vrací DIR_item přes pointer out_dir_item.

   Proměnné:
            inode        - inode přes který se přistoupí k DIR_ITEMU ve kterém se bude hledat
            item_name    - jméno souboru který se hledá v directory
            out_dir_item - pointer přes který se vrací nalezený soubor

   Vrací return_code:
                    CODE_OK              - Soubor byl v inodu nalezen
                    CODE_PATH_NOT_FOUND  - Soubor nebyl nalezen
                    CODE_FATAL_ERROR     - Při operaci nastala chyba při práci s pamětí
 */
return_code utils_get_inode_item(INODE *inode, char *item_name, DIR_ITEM *out_dir_item) {
    int i;
    DIR_ITEM *items_in_directory;
    INODE dir_inode;
    bool find = false;

    if (item_name == NULL) {
        return CODE_PATH_NOT_FOUND;
    }

    fs_seek_set_i(inode->nodeid);
    fread(&dir_inode, sizeof(INODE), 1, global_filesystem);

    items_in_directory = malloc(sizeof(DIR_ITEM) * dir_inode.references);

    if (items_in_directory == NULL) {
        return CODE_FATAL_ERROR;
    }
    fs_seek_set_c(dir_inode.direct1);
    fread(items_in_directory, sizeof(DIR_ITEM), dir_inode.references, global_filesystem);
    for (i = 0; i < dir_inode.references; i++) {
        if (strncmp(items_in_directory[i].item_name, item_name, strlen(item_name)) == 0) {
            out_dir_item->inode = items_in_directory[i].inode;
            strncpy(out_dir_item->item_name, items_in_directory[i].item_name, MAX_FILE_NAME);
            find = true;
            break;
        }
    }

    free(items_in_directory);

    if (find == false) {
        return CODE_PATH_NOT_FOUND;
    }

    return CODE_OK;
}


/*
   V předaném DIR_item hledá jestli obsahuje soubor o zadaném jméně.
   Pokud ano vrací DIR_item přes pointer out_dir_item.

   Proměnné:
            directory    - adresář ve kterém se hledá jestli obsahuje soubor o zadaném jménu
            item_name    - jméno souboru který se hledá v directory
            out_dir_item - pointer přes který se vrací nalezený soubor

   Vrací return_code:
                    CODE_OK              - Soubor byl v inodu nalezen
                    CODE_PATH_NOT_FOUND  - Soubor nebyl nalezen
                    CODE_FATAL_ERROR     - Při operaci nastala chyba při práci s pamětí
 */
return_code utils_get_dir_item(DIR_ITEM *directory, char *item_name, DIR_ITEM *out_dir_item) {
    INODE *inode;
    return_code result = CODE_OK;

    inode = malloc(sizeof(INODE));

    if (inode == NULL) {
        return CODE_FATAL_ERROR;
    }

    utils_load_inode(inode, directory->inode);
    result = utils_get_inode_item(inode, item_name, out_dir_item);
    free(inode);
    return result;
}


/*
   Metoda zkontroluje jestli je v clusteru adresáře ještě místo pro vytvoření dalšího filu a zároveň
   vrátí offset adresy v clusteru přes pointer, kam se může zapsat adresář.

   Proměnné:
            dir          - dir item ve kterém zjišťujeme jestli je místo
            offset       - offset ukazující na začátek volného místa kam se může zapsat nový DIR_ITEM.
                           Hodnota vrácena přes poinet.
            has_space    - přes tuto proměnou se vrací -1 pokud už místo není nebo 0 pokud je.

   Vrací return_code:
                    CODE_OK              - Má ještě místo
                    CODE_FATAL_ERROR     - Při operaci nastala chyba při práci s pamětí
 */
return_code utils_has_dir_space(DIR_ITEM *dir, int32_t *offset, int *has_space) {
    int max_files_in_dir;
    INODE *inode;

    fs_seek_set_i(dir->inode);
    inode = malloc(sizeof(INODE));

    if (inode == NULL) {
        return CODE_FATAL_ERROR;
    }

    fread(inode, sizeof(INODE), 1, global_filesystem);


    max_files_in_dir = CLUSTER_SIZE / sizeof(DIR_ITEM); /* CLUSTER_SIZE bude vždcky větší než 32B a zaroveň dělitelná 16 */
    /* z počtu ukazatalů na file se dá zjistit kolik je věcí v adresáři */
    /* sice ukazet .. nemusí ukazovat na aktuální adresář ale to vykrývá refence adresáře v adresáři o úroveň níže */
    if (inode->references >= max_files_in_dir) {
        *has_space = -1;
    } else {
        /* directech je uložené číslo clusteru ne jeho adresa */
        *offset = (global_sb->data_start_address + (inode->direct1 * CLUSTER_SIZE)) +
                  (sizeof(DIR_ITEM) * inode->references);
        *has_space = 0;
    }

    free(inode);

    return CODE_OK;
}



/*
   Zvýší nebo sníži počet referencí na inode

   Proměnné:
            inode - id inodu
            op    - pokud je true zvýší počet referencí na inode, pokud false sníží počet referencí na inode.
                    zvyšuje nebo zmenšuje se vždy o 1.
 */
void utils_modify_inode_references(INODE *inode, bool op) {

    if (op == true) {
        inode->references++;
    } else {
        inode->references--;
    }

    fs_seek_set_i(inode->nodeid);
    fwrite(inode, sizeof(INODE), 1, global_filesystem);

}


/*
   Zapíše referenci složky do aktuálního adresáře

   Proměnné:
            parent    - Rodičovský adresář do kterého se zapíše reference
            dir_item  - DIR_ITEM který se má zapsat
            offset    - offset na který se má zapsat ve filesystému

   Vrací return_code:
                    CODE_OK              - Vše proběhlo v pořádku
                    CODE_FATAL_ERROR     - Při operaci nastala chyba při práci s pamětí
 */
return_code utils_write_file_reference_to_dir(DIR_ITEM *parent, DIR_ITEM *dir_item, int32_t *offset) {
    INODE *cur_dir_inode;

    cur_dir_inode = malloc(sizeof(INODE));

    if (cur_dir_inode == NULL) {
        return CODE_FATAL_ERROR;
    }

    fs_seek_set_i(parent->inode);
    fread(cur_dir_inode, sizeof(INODE), 1, global_filesystem);

    utils_modify_inode_references(cur_dir_inode, true); /* zvýší reference na inode aktuální adresáře */

    fs_seek_set(*offset);
    fwrite(dir_item, sizeof(DIR_ITEM), 1, global_filesystem);

    free(cur_dir_inode);

    return CODE_OK;
}


/*
   Zkontroluje jestli je adresář prázdný. Výsledek se vrací přes pointer out_is_empty, kam
   je uložená hodnota výsledku.

   Proměnné:
            directory    - zkoumaný adresář
            out_is_empty - pointer na výsledek. 0 adresář je prázný, -1 adresář není prázdný.

   Vrací return_code:
                    CODE_OK              - Má ještě místo
                    CODE_FATAL_ERROR     - Při operaci nastala chyba při práci s pamětí
 */
return_code utils_is_directory_empty(DIR_ITEM *directory, int *out_is_empty) {
    INODE *dir_inode;

    dir_inode = malloc(sizeof(INODE));

    if (dir_inode == NULL) {
        return CODE_FATAL_ERROR;
    }

    fs_seek_set_i(directory->inode);
    fread(dir_inode, sizeof(INODE), 1, global_filesystem);

    if (dir_inode->references > 2) {
        *out_is_empty = -1;
    } else {
        *out_is_empty = 0;
    }

    free(dir_inode);

    return CODE_OK;
}


/*
   Vyčistí reference nepřímých odkazů prvního řádu

   Proměnné:
            inode             - inode který se má čistit
            clusters          - počet zbývajících clusterů
            processed_cluster - počet zpracovaných (uvolněných clusterů)
 */
void free_firts_indirect_clusters(INODE *inode, int *clusters, int *processed_cluster) {
    int cluster_id, i ;
    int num_clusters_to_free;
    int num_of_indirect_links = CLUSTER_SIZE / sizeof (int32_t);

    if((*clusters) >= num_of_indirect_links){
        num_clusters_to_free = num_of_indirect_links;
    }else{
        num_clusters_to_free = (*clusters);
    }

    for(i = 0; i < num_clusters_to_free; i++){
        cluster_id = fs_seek_set_in1_c(inode->indirect1, i, false, false);
        //fread(&cluster_id, sizeof (int32_t), 1, global_filesystem);
        utils_free_cluster(cluster_id);
        (*clusters)--;
        (*processed_cluster)++;
    }

    utils_free_cluster(inode->indirect1);
    (*clusters)--;
    (*processed_cluster)++;
}


/*
   Vyčistí reference nepřímých odkazů druhého řádu

   Proměnné:
            inode             - inode který se má čistit
            clusters          - počet zbývajících clusterů
            processed_cluster - počet zpracovaných (uvolněných clusterů)
 */
void free_second_indirect_clusters(INODE *inode, int *clusters, int *processed_cluster) {
    int cluster_id, i, j, temp_processed = 0;
    /*-2 odkazy na první indirect a druhý indirect link */
    int number_of_clusters = utils_calc_needed_cluster_for_file(inode->file_size);
    int number_of_indirect = CLUSTER_SIZE / sizeof(int32_t);
    int number_of_second_indirect = (int)ceil((number_of_clusters - number_of_indirect - 5) / (double )(number_of_indirect));
    int num_clusters_to_process = (*clusters) - number_of_second_indirect - 1;




    for(i = 0; i < number_of_second_indirect; i++){
        for(j = 0; j < (CLUSTER_SIZE / sizeof(int32_t)); j++) {
            if(temp_processed >= num_clusters_to_process){
                break;
            }
            //first_indir = i / (CLUSTER_SIZE / sizeof(int32_t));
            cluster_id = fs_seek_set_in2_c(inode->indirect2, i, j, false, false);
            //fread(&cluster_id, sizeof(int32_t), 1, global_filesystem);
            utils_free_cluster(cluster_id);
            (*clusters)--;
            (*processed_cluster)++;
            temp_processed++;


        }
    }

    for(i = 0; i < number_of_second_indirect; i++){
        cluster_id = fs_seek_set_in1_c(inode->indirect2, i, false, false);
      //  fread(&cluster_id, sizeof (int32_t), 1, global_filesystem);
        utils_free_cluster(cluster_id);
        (*clusters)--;
        (*processed_cluster)++;
    }

    utils_free_cluster(inode->indirect2);
    (*clusters)--;
    (*processed_cluster)++;

}


/*
   Vyčistí reference v první pětici direct odkazů

   Proměnné:
            inode             - inode který se má čistit
            clusters          - počet zbývajících clusterů
            processed_cluster - počet zpracovaných (uvolněných clusterů)
 */
void free_direct_cluster(INODE *inode, int *clusters, int *processed_cluster) {

    if (*clusters > 0) {
        utils_free_cluster(inode->direct1);
        (*clusters)--;
        (*processed_cluster)++;
    }
    if (*clusters > 0) {
        utils_free_cluster(inode->direct2);
        (*clusters)--;
        (*processed_cluster)++;
    }
    if (*clusters > 0) {
        utils_free_cluster(inode->direct3);
        (*clusters)--;
        (*processed_cluster)++;
    }
    if (*clusters > 0) {
        utils_free_cluster(inode->direct4);
        (*clusters)--;
        (*processed_cluster)++;
    }
    if (*clusters > 0) {
        utils_free_cluster(inode->direct5);
        (*clusters)--;
        (*processed_cluster)++;
    }
}


/*
   Zpracuje předanou cestu a vrátí odkaz na DIR_ITEM, který je na konci cesty přes out_dir_item.
   Umí zpracovat jak relativní tak absulutní cestu

   Proměnné:
            path         - zpracovávaná cesta
            out_dir_item - DIR_ITEM nalezený v předané cestě

   Vrací return_code:
                    CODE_OK              - Má ještě místo
                    CODE_FATAL_ERROR     - Při operaci nastala chyba při práci s pamětí
                    CODE_PATH_NOT_FOUND  - Soubor nebyl nalezen
 */
return_code utils_process_path(char *path, DIR_ITEM *out_dir_item) {
    int i = 0;
    int cur_dir_it_name_index = 0;
    char dir_name_buff[MAX_FILE_NAME];
    return_code result = CODE_OK;

    memset(dir_name_buff, 0, MAX_FILE_NAME);

    DIR_ITEM *temp_item = malloc(sizeof(DIR_ITEM));
    INODE *cur_dir_inode = malloc(sizeof(INODE));


    if (temp_item == NULL || cur_dir_inode == NULL) {
        return CODE_FATAL_ERROR;
    }


    if (path[0] == '/') {
        utils_load_inode(cur_dir_inode, 1); /* kořenový adresář má vždy první inode v pořadí */
        if ((result = utils_get_inode_item(cur_dir_inode, ".\0", temp_item)) == CODE_FATAL_ERROR) {
            return result;
        }
        i++;

    }else if (path[0] == '\0'){
        utils_copy_dir_item(global_current_dir, temp_item);
    }else {
        utils_load_inode(cur_dir_inode, global_current_dir->inode);
    }

    for (; i < strlen(path); i++) {
        if ((path[i] == '/' && i != 0) || i == strlen(path) - 1) {
            if (strlen(path) == 1 || path[i] != '/') {
                dir_name_buff[cur_dir_it_name_index] = path[i];
                dir_name_buff[cur_dir_it_name_index + 1] = '\0';
            }

            if ((result = utils_get_inode_item(cur_dir_inode, dir_name_buff, temp_item)) != CODE_OK) {
                break;
            }
            utils_load_inode(cur_dir_inode, temp_item->inode);
            memset(dir_name_buff, '\0', MAX_FILE_NAME);
            cur_dir_it_name_index = 0;
        } else {
            dir_name_buff[cur_dir_it_name_index] = path[i];
            cur_dir_it_name_index++;
        }
    }

    out_dir_item->inode = temp_item->inode;
    strncpy(out_dir_item->item_name, temp_item->item_name, MAX_FILE_NAME);

    free(temp_item);
    free(cur_dir_inode);

    return result;
}


/*
   Metoda přiřadí předané volné clustery k inodu

   Proměnné:
            inode         - inode kteru se přiřazují clustery
            free_clusters - pole volných clusterů
            cluster_num   - počet volných clusterů
 */
void utils_assign_clusters_to_inode(INODE *inode, int32_t *free_clusters, int number_of_clusters) {
    int i, j;
    int number_of_indirect = CLUSTER_SIZE / sizeof(int32_t);
    int number_of_second_indir = (int)ceil((number_of_clusters - number_of_indirect - 5) / (double )(number_of_indirect));
    int processed_clusters = 0;

    if (number_of_clusters > 0) {
        inode->direct1 = free_clusters[processed_clusters++];
    }

    if (number_of_clusters > 1) {
        inode->direct2 = free_clusters[processed_clusters++];
    }

    if (number_of_clusters > 2) {
        inode->direct3 = free_clusters[processed_clusters++];
    }

    if (number_of_clusters > 3) {
        inode->direct4 = free_clusters[processed_clusters++];
    }

    if (number_of_clusters > 4) {
        inode->direct5 = free_clusters[processed_clusters++];
    }

    if (number_of_clusters > 5) {
        inode->indirect1 = free_clusters[processed_clusters++];
        fs_seek_set_c(inode->indirect1);
        for (i = 0; i < number_of_indirect; i++) {
            fwrite(&free_clusters[processed_clusters++], sizeof(int32_t), 1, global_filesystem);
            if (processed_clusters >= number_of_clusters) {
                break;
            }
        }
    }

//    number_of_clusters--;
    if (number_of_clusters >
        (number_of_indirect + 5 /*- 1*/)) { /* 5 počet directů -1 protože je číslování od 0  */
        inode->indirect2 = free_clusters[processed_clusters++];
        fs_seek_set_c(inode->indirect2);
        for (i = 0; i < number_of_second_indir; i++) {
            fwrite(&free_clusters[processed_clusters++], sizeof(int32_t), 1, global_filesystem);
        }

        for (i = 0; i < number_of_second_indir; i++) {
            fs_seek_set_in1_c(inode->indirect2, i, false, false);
            for (j = 0; j < number_of_indirect; j++) {
                fwrite(&free_clusters[processed_clusters++], sizeof(int32_t), 1, global_filesystem);
                if (processed_clusters >= number_of_clusters) {
                    //cluster_processed = true;
                    break;
                }
            }

        }
    }
}


/*
   Nastavuje seek pro nepřímý odkaz prvního řádu

   Proměnné:
            inode   - inode do jehož clusterů se zapisuje
            cluster - id clusteru kam se má zapsat
            byte    - n-tý zpracovávaný byte v clusteru
 */
void utils_seek_first_indirect(INODE *inode, int cluster, int byte) {
    int cluster_id = cluster - 5; /* 5 == počet indirect linků; proměnná pro zjištění na jaké jsem pozici indirect inodu */
    if (byte == 0) {
        fs_seek_set_in1_c(inode->indirect1, cluster_id, false, false);
    }
}


/*
   Nastavuje seek pro nepřímý odkaz druhého řádu

   Proměnné:
            inode   - inode do jehož clusterů se zapisuje
            cluster - id clusteru kam se má zapsat
            byte    - n-tý zpracovávaný byte v clusteru
 */
void utils_seek_second_indirect(INODE *inode, int cluster, int byte) {
    /* 5 == počet indirect linků; proměnná pro zjištění na jaké jsem pozici indirect inodu */
    int cluster_id = cluster - (CLUSTER_SIZE / sizeof(int32_t)) - 5;
    int first_indir = cluster_id / (CLUSTER_SIZE / sizeof(int32_t));
    cluster_id = cluster_id % 16;
    if (byte == 0) {
        fs_seek_set_in2_c(inode->indirect2, first_indir, cluster_id, false, false);
    }
}


/*
   Metoda zapisuje předaný byte do příslušného clusteru

   Proměnné:
            inode   - inode který ukazuje na data
            data    - data k zapsání
            cluster - n-ty cluster do nejž se bude zapisovat
            bytes    - n-ty počet bytů co se má zapsat
 */
void utils_write_byte_to_links(INODE *inode, char *data, int cluster, int bytes) {
    int first_in = 5 + (CLUSTER_SIZE / sizeof(int32_t));

    if (cluster == 0) {
            fs_seek_set_c(inode->direct1);
    } else if (cluster == 1) {
            fs_seek_set_c(inode->direct2);
    } else if (cluster == 2) {
            fs_seek_set_c(inode->direct3);
    } else if (cluster == 3) {
            fs_seek_set_c(inode->direct4);
    } else if (cluster == 4) {
            fs_seek_set_c(inode->direct5);
    } else if (cluster < first_in) {
        utils_seek_first_indirect(inode, cluster, 0);
    } else {
        utils_seek_second_indirect(inode, cluster, 0);
    }

    fwrite(data, sizeof(char), bytes, global_filesystem);
}

/*
   Metoda čte data z linků přiřezených inodu

   Proměnné:
            inode            - inode který ukazuje na data
            cluster          - n-ty cluster z nejž se bude číst
            byte_num_to_read - počet bytů kolik se bude číst
            out_data         - pointer na načtená data
 */
void utils_read_from_links(INODE *inode, int cluster, int byte_num_to_read, char *out_data){
    int first_in = 5 + (CLUSTER_SIZE / sizeof(int32_t));

    if (cluster == 0) {
            fs_seek_set_c(inode->direct1);
    } else if (cluster == 1) {
            fs_seek_set_c(inode->direct2);
    } else if (cluster == 2) {
            fs_seek_set_c(inode->direct3);
    } else if (cluster == 3) {
            fs_seek_set_c(inode->direct4);
    } else if (cluster == 4) {
            fs_seek_set_c(inode->direct5);
    } else if (cluster < first_in) {
        utils_seek_first_indirect(inode, cluster, 0);
    } else {
        utils_seek_second_indirect(inode, cluster, 0);
    }

    fread(out_data, sizeof(char), byte_num_to_read, global_filesystem);
    out_data[byte_num_to_read] = '\0';
}


/*
   Metoda vypíše informaci o inodu s předaným id.

   Proměnné:
            inode_id - id inodu o kterém se má vypsat info.
 */
void utils_print_inode_info(int inode_id) {
    int i, j, number_of_clusters;
    int number_of_indirect = CLUSTER_SIZE / sizeof(int32_t);
    int processed_clusters = 0;
    int extra_clusters = 0;
    bool cluster_processed = false;

    if (inode_id < 1 || inode_id > (*global_sb).inode_count) {
        printf("Out of range!\n");
    } else {
        INODE *inode = malloc(sizeof(INODE));
        utils_load_inode(inode, inode_id);
        number_of_clusters = utils_calc_needed_cluster_for_file(inode->file_size);
        extra_clusters = number_of_clusters - (int) ceil((double) inode->file_size / CLUSTER_SIZE);
        number_of_clusters -= extra_clusters; /* posunutí čítače o inref linky */
        printf("Inode-ID    : %d\n", inode->nodeid);
        printf("Inode-size  : %d\n", inode->file_size);
        printf("Inode-ref   : %d\n", inode->references);
        printf("Inode-is dir: %d\n", inode->isDirectory);
        if (number_of_clusters > 0) {
            printf("Inode-ref1  : %d\n", inode->direct1);
        }

        if (number_of_clusters > 1) {
            printf("Inode-ref2  : %d\n", inode->direct2);
        }

        if (number_of_clusters > 2) {
            printf("Inode-ref3  : %d\n", inode->direct3);
        }

        if (number_of_clusters > 3) {
            printf("Inode-ref4  : %d\n", inode->direct4);
        }

        if (number_of_clusters > 4) {
            printf("Inode-ref5  : %d\n", inode->direct5);
        }

        processed_clusters = 5;
        if (number_of_clusters > 5) {
            printf("Inode-inref1: %d\n", inode->indirect1);
            for (i = 0; i < number_of_indirect; i++) {
                fs_seek_set_in1_c(inode->indirect1, i, true, false);
                processed_clusters++;
                if (processed_clusters >= number_of_clusters) {
                    break;
                }
            }
        }

        if (number_of_clusters >
            (number_of_indirect + 5)) { /* 5 počet directů -1 protože je číslování od 0  */
            printf("Inode-inref2: %d\n", inode->indirect2);
            for (i = 0; i < number_of_indirect && cluster_processed == false; i++) {
                for (j = 0; j < number_of_indirect && cluster_processed == false; j++) {
                    if (processed_clusters >= number_of_clusters) {
                        cluster_processed = true;
                        break;
                    }
                    if (j == 0) {
                        fs_seek_set_in2_c(inode->indirect2, i, j, true, true);
                    } else {
                        fs_seek_set_in2_c(inode->indirect2, i, j, true, false);
                    }

                    processed_clusters++;

                }
            }
        }


        free(inode);
    }
}


/*
   Metoda má za úkol najít DIR_ITEM aktuální adresáře v rodičovském adresáři.

   Proměnné:
            cur_dir - funguje jako proměná pro níž sa má najít DIR_ITEM se stejným id inodu v nadřazenám adresáři a
                      zároveň se do ní výsledek uloží a vrací se přes pointer.

   Vrací return_code:
                    CODE_OK              - Adresář byl nalezen
                    CODE_FATAL_ERROR     - Při operaci nastala chyba při práci s pamětí
                    CODE_PATH_NOT_FOUND  - Soubor nebyl nalezen
 */
return_code utils_find_cur_dir_level_up(DIR_ITEM *cur_dir) {
    int i;
    DIR_ITEM *items_in_directory;
    DIR_ITEM *dir_level_up;
    DIR_ITEM *dir_double_level_up;
    INODE *dir_inode;
    return_code result;

    dir_double_level_up = malloc(sizeof(DIR_ITEM));
    dir_level_up = malloc(sizeof(DIR_ITEM));
    dir_inode = malloc(sizeof(INODE));

    if (dir_level_up == NULL) {
        return CODE_FATAL_ERROR;
    }

    if ((result = utils_get_dir_item(cur_dir, "..\0", dir_level_up)) == CODE_OK) {
        utils_load_inode(dir_inode, dir_double_level_up->inode);
        items_in_directory = malloc(sizeof(DIR_ITEM) * dir_inode->references);

        fs_seek_set_c(dir_inode->direct1);
        fread(items_in_directory, sizeof(DIR_ITEM), dir_inode->references, global_filesystem);

        for (i = 0; i < dir_inode->references; i++) {
            if (items_in_directory[i].inode == cur_dir->inode) {
                utils_copy_dir_item(&items_in_directory[i], cur_dir);
                break;
            }
        }

        free(items_in_directory);

    }

    free(dir_double_level_up);
    free(dir_level_up);
    free(dir_inode);
    return result;
}


/*
   Metoda má za úkol najít rodičovský DIR_ITEM vloženého adresáře cur_dir.

   Proměnné:
            cur_dir          - aktuální adresář pro nejž se bude hledat rodičovský adresář.
            out_dir_level_up - slouží pro výsledný rodičovský adresář, který je předán přes pointer

   Vrací return_code:
                    CODE_OK              - Adresář byl nalezen
                    CODE_FATAL_ERROR     - Při operaci nastala chyba při práci s pamětí
                    CODE_PATH_NOT_FOUND  - Soubor nebyl nalezen
 */
return_code utils_find_dir_level_up(DIR_ITEM *cur_dir, DIR_ITEM *out_dir_level_up) {
    int i;
    DIR_ITEM *items_in_directory;
    DIR_ITEM *dir_level_up;
    DIR_ITEM *dir_double_level_up;
    INODE *dir_inode;
    return_code result;

    dir_double_level_up = malloc(sizeof(DIR_ITEM));
    dir_level_up = malloc(sizeof(DIR_ITEM));
    dir_inode = malloc(sizeof(INODE));

    if (dir_level_up == NULL) {
        return CODE_FATAL_ERROR;
    }

    if ((result = utils_get_dir_item(cur_dir, "..\0", dir_level_up)) == CODE_OK) {
        if ((result = utils_get_dir_item(dir_level_up, "..\0", dir_double_level_up)) == CODE_OK) {
            utils_load_inode(dir_inode, dir_double_level_up->inode);
            items_in_directory = malloc(sizeof(DIR_ITEM) * dir_inode->references);

            fs_seek_set_c(dir_inode->direct1);
            fread(items_in_directory, sizeof(DIR_ITEM), dir_inode->references, global_filesystem);

            for (i = 0; i < dir_inode->references; i++) {
                if (items_in_directory[i].inode == dir_level_up->inode) {
                    out_dir_level_up->inode = items_in_directory[i].inode;
                    strncpy(out_dir_level_up->item_name, items_in_directory[i].item_name, MAX_FILE_NAME);
                    break;
                }
            }

            free(items_in_directory);
        }

    }

    free(dir_double_level_up);
    free(dir_level_up);
    free(dir_inode);
    return result;
}


/*
   Metoda oddělí ze zdrojové cesty část pro adresář souboru a část názvu souboru. Tyto hodnoty jsou
   pak vráceny přes pointery

   Proměnné:
            source        - Zdrojová cesta se kterou se pracuje
            out_dir_path  - cesta do složky. Vraceno přes pointer
            out_file_name - Jméno souboru. Vraceno přes pointer
 */
void utils_intersect_directory_path_and_file_name(char *source, char *out_dir_path, char *out_file_name){
    int start = 0, source_len, i;

    source_len = strlen(source);

    if(source[source_len - 1] == '/'){
        source[source_len - 1] = '\0';
    }

    for(i = source_len; i > 0; i--){
        if(source[i - 1] == '/'){
            start = i;
            break;
        }
    }

    strncpy(out_file_name, source + start, source_len - start);
    out_file_name[source_len - start] = '\0';

    if(start != 0){
        strncpy(out_dir_path, source, source_len - (source_len - start));
        out_dir_path[source_len - (source_len - start) - 1] = '\0'; /* -1 abych se zbavil poslední '\'... Jen pro jistotu */
    }else{
        out_dir_path[0] = '\0';
    }


}



/*
   Metoda překopíruje data zdrojového (source) DIR_ITEM do cílového (dest) DIR_ITEMU

   Proměnné:
            source - zdrojový DIR_ITEM, ze kterého si získají data.
            dest   - cílový DIR_ITEM, do kterého se přepíšou data
 */
void utils_copy_dir_item(DIR_ITEM *source, DIR_ITEM *dest) {
    dest->inode = source->inode;
    strncpy(dest->item_name, source->item_name, MAX_FILE_NAME);
}

/*
   Metoda odstraní inode a zároveň uvolní jeho přidělené clustery.

   Proměnné:
            dir_inode - inode který se bude mazat
 */
void utils_remove_inode(INODE *dir_inode) {
    int used_clusters = utils_calc_needed_cluster_for_file(dir_inode->file_size);
    int processed_clusters = 0;


    free_direct_cluster(dir_inode, &used_clusters, &processed_clusters);
    if(used_clusters > 0){
        free_firts_indirect_clusters(dir_inode, &used_clusters, &processed_clusters);
    }

    if(used_clusters > 0){
        free_second_indirect_clusters(dir_inode, &used_clusters, &processed_clusters);
    }

    utils_free_inode(dir_inode->nodeid);
    fs_seek_set_i(dir_inode->nodeid);
    dir_inode->nodeid = 0;
    dir_inode->file_size = 0;
    dir_inode->references = 0;
    dir_inode->direct1 = 0;
    dir_inode->direct2 = 0;
    dir_inode->direct3 = 0;
    dir_inode->direct4 = 0;
    dir_inode->direct5 = 0;
    dir_inode->indirect1 = 0;
    dir_inode->indirect2 = 0;
    fwrite(dir_inode, sizeof (INODE), 1 , global_filesystem);
}


/*
   Metoda odstaní file ze zadaného souboru a zároveň odstraní všechna data filu pokud je nastavená proměnná
   romeve_inode na true.

   Proměnné:
            dir_inode         - inode file, který se bude mazat pokud je tak nastaveno
            parrent_dir_inode - rodičovská directory ze které se smaže reference
            file_name         - jméno DIR_item která je uložená v rodičovském inodu
            remove_inode      - logická proměnná, kdy true znamená že se má smazat inode filu a false že se inode mazat nemá

   Vrací return_code:
                    CODE_OK              - Vše v pořádku
                    CODE_FATAL_ERROR     - Při operaci nastala chyba při práci s pamětí
 */
return_code utils_remove_dir_reference(INODE *dir_inode, INODE *parrent_dir_inode, char *file_name, bool remove_inode){
    int dir_item_id;
    DIR_ITEM *dir_items;
    int32_t offset;

    dir_items = calloc(sizeof(DIR_ITEM), parrent_dir_inode->references);

    if (dir_items == NULL) {
        return CODE_FATAL_ERROR;
    }

    /* Přečtení souborů v rodičovkém adresáří požadovaného adresáře*/
    fs_seek_set_c(parrent_dir_inode->direct1);
    fread(dir_items, sizeof(DIR_ITEM), parrent_dir_inode->references, global_filesystem);

    fs_seek_set_c(parrent_dir_inode->direct1);
    offset = ftell(global_filesystem);
    /*Přepsání rodičovksého adresáře*/
    for (dir_item_id = 0; dir_item_id < parrent_dir_inode->references; dir_item_id++) {
        if (strncmp(dir_items[dir_item_id].item_name, file_name, strlen(file_name)) == 0
            && strlen(dir_items[dir_item_id].item_name) == strlen(file_name)) {
            /*smazání inodu. Data netřeba přepisovat. Bez reference je může cokoliv přepsat*/
            if(remove_inode == true){
                utils_remove_inode(dir_inode);
            }
        } else {
            /*zapisuje od začátku clusteru a fseek se automaticky posunuje*/

            fs_seek_set(offset);
            fwrite(&dir_items[dir_item_id], sizeof(DIR_ITEM), 1, global_filesystem);
            offset += sizeof (DIR_ITEM);
        }
    }

    /*snižení počtu referencí v rodičovské adresáři*/
    utils_modify_inode_references(parrent_dir_inode, false);
    free(dir_items);

    return CODE_OK;
}


/*
   Metoda zkontroluje zda má inode přímý linky v pohodě.

   Proměnné:
            inode              - Inode pro který se kontrolují linky
            processed_clusters - pointer na počet již zpracovaných clusterů
            cluster_count      - celkový počet clusterů

   Vrací int, kdy 0 znamená že linky jsou v pořádku podle testu. -1 někde se stala chyba.
 */
int check_direct_links(INODE *inode, int *processed_clusters, int cluster_count, int *cluster_arr){

    int max_cluster_id = (*global_sb).cluster_count;

    if((*processed_clusters) < cluster_count){
        if(inode->direct1 < 0 || inode->direct1 > max_cluster_id){
            printf("Inode with id %d has corruptet direct link 1\n", inode->nodeid );
            (*processed_clusters)++;
        }else{
            cluster_arr[inode->direct1]++;
            (*processed_clusters)++;
        }
    }

    if((*processed_clusters) < cluster_count){
        if(inode->direct2 < 0 || inode->direct2 > max_cluster_id){
            printf("Inode with id %d has corruptet direct link 2\n", inode->nodeid );
            (*processed_clusters)++;
        }else{
            cluster_arr[inode->direct2]++;
            (*processed_clusters)++;
        }
    }

    if((*processed_clusters) < cluster_count){
        if(inode->direct3 < 0 || inode->direct3 > max_cluster_id){
            printf("Inode with id %d has corruptet direct link 3\n", inode->nodeid );
            (*processed_clusters)++;
        }else{
            cluster_arr[inode->direct3]++;
            (*processed_clusters)++;
        }
    }

    if(*processed_clusters < cluster_count){
        if(inode->direct4 < 0 || inode->direct4 > max_cluster_id){
            printf("Inode with id %d has corruptet direct link 4\n", inode->nodeid );
            (*processed_clusters)++;
        }else{
            cluster_arr[inode->direct4]++;
            (*processed_clusters)++;
        }
    }

    if((*processed_clusters) < cluster_count){
        if(inode->direct5 < 0 || inode->direct5 > max_cluster_id){
            printf("Inode with id %d has corruptet direct link 5\n", inode->nodeid );
            (*processed_clusters)++;
        }else{
            cluster_arr[inode->direct5]++;
            (*processed_clusters)++;
        }
    }

    return 0;

}


/*
   Metoda zkontroluje zda má inode nepřímé linky prvního řádu v pohodě

   Proměnné:
            inode              - Inode pro který se kontrolují linky
            processed_clusters - pointer na počet již zpracovaných clusterů
            cluster_count      - celkový počet clusterů

   Vrací int, kdy 0 znamená že linky jsou v pořádku podle testu. -1 někde se stala chyba.
 */
int check_first_indirect(INODE *inode, int *processed_clusters, int cluster_count, int *cluster_arr){
    int i, indirect_clusters;
    int max_cluster_id = (*global_sb).cluster_count;
    int cluster_to_process = cluster_count - (*processed_clusters) + 1; //+1 protože se následně spracuje první indirect
    int indirect_links = CLUSTER_SIZE / sizeof (int32_t);
    int32_t temp_cluster_id;

    (*processed_clusters)++;
    if(inode->indirect1 < 0 || inode->indirect1 > max_cluster_id){
        printf("Inode with id %d has corruptet indirect 1\n", inode->nodeid );
        (*processed_clusters)++;
    }else{
        cluster_arr[inode->indirect1]++;
    }

    if(cluster_to_process <= (*processed_clusters)){
        indirect_clusters = cluster_to_process;
    }else{
        indirect_clusters = indirect_links;
    }

    for(i = 0; i < indirect_clusters; i++){
        temp_cluster_id = fs_seek_set_in1_c(inode->indirect1, i, false, false);

        if(temp_cluster_id < 0 || temp_cluster_id > max_cluster_id){
            printf("Inode with id %d has corruptet indirect 1\n", temp_cluster_id);
            (*processed_clusters)++;
        }else{
            cluster_arr[temp_cluster_id]++;
        }
        (*processed_clusters)++;

    }

    return 0;

}


/*
   Metoda zkontroluje zda má inode nepřímé linky druhého řádu zda jsou v pohodě

   Proměnné:
            inode              - Inode pro který se kontrolují linky
            processed_clusters - pointer na počet již zpracovaných clusterů
            cluster_count      - celkový počet clusterů

   Vrací int, kdy 0 znamená že linky jsou v pořádku podle testu. -1 někde se stala chyba.
 */
int check_second_indirect(INODE *inode, int *processed_clusters, int cluster_count, int *cluster_arr){
    int i, j;
    int max_cluster_id = (*global_sb).cluster_count;
    int number_of_first_indir = ceil((double)(cluster_count - (*processed_clusters)) / (CLUSTER_SIZE / sizeof (int32_t)));
    int indirect_links = CLUSTER_SIZE / sizeof (int32_t);
    int32_t temp_id;


    if(inode->indirect2 < 0 || inode->indirect2 > max_cluster_id){
        (*processed_clusters)++;
    }else{
        cluster_arr[inode->indirect2]++;
    }

    (*processed_clusters)++;

    /* Kontrola nepřímých linků prvního stupně */

    for(i = 0; i < number_of_first_indir; i++){
        temp_id = fs_seek_set_in1_c(inode->indirect2, i , false, false);
        if(temp_id < 0 || temp_id > max_cluster_id){
            return -1;
        }else{
            cluster_arr[temp_id]++;
        }
        (*processed_clusters)++;
    }


    for(i = 0; i < number_of_first_indir; i++){
        for(j = 0; j < indirect_links; j++){
           temp_id =  fs_seek_set_in2_c(inode->indirect2, i, j, false, false);
            if(temp_id < 0 || temp_id > max_cluster_id){
                printf("Inode with id %d has corruptet indirect %d - %d - %d\n", inode->nodeid, inode->indirect2, j, i);
                (*processed_clusters)++;
            }else{
                cluster_arr[temp_id]++;
            }

            (*processed_clusters)++;
            if((*processed_clusters) == cluster_count){
                break;
            }
        }

    }

    return 0;

}

/*
   Metoda odstaní file ze zadaného souboru a zároveň odstraní všechna data filu pokud je nastavená proměnná
   romeve_inode na true.

   Proměnné:
            dir_inode         - inode file, který se bude mazat pokud je tak nastaveno
            parrent_dir_inode - rodičovská directory ze které se smaže reference
            file_name         - jméno DIR_item která je uložená v rodičovském inodu
            remove_inode      - logická proměnná, kdy true znamená že se má smazat inode filu a false že se inode mazat nemá

   Vrací return_code:
                    CODE_OK              - Vše v pořádku
                    CODE_FATAL_ERROR     - Při operaci nastala chyba při práci s pamětí
 */
return_code utils_check_inodes_consistency() {
    INODE *temp_inode;
    int i, cur_inode_id, number_of_clusters;
    int used_inodes = (*global_sb).inode_count - calc_number_of_free_inodes();
    int inode_ids_arr[used_inodes];
    int cluster_arr[(*global_sb).cluster_count];
    int processed_clusters;
    int check_res;
    find_used_inodes(inode_ids_arr);

    for(i = 0; i < (*global_sb).cluster_count; i++){
        cluster_arr[i] = 0;
    }

    find_used_clusters(cluster_arr);

    temp_inode = malloc(sizeof(INODE));

    if (temp_inode == NULL) {
        return CODE_FATAL_ERROR;
    }

    for (i = 0; i < used_inodes; i++) {
        processed_clusters = 0;
        cur_inode_id = inode_ids_arr[i];
        utils_load_inode(temp_inode, cur_inode_id);
        number_of_clusters = utils_calc_needed_cluster_for_file(temp_inode->file_size);

        //Kontrola přímých
        check_res = check_direct_links(temp_inode, &processed_clusters, number_of_clusters, cluster_arr);
        if (check_res != 0) {
            break;
        }
        //Kontrola prvních nepřímých
        if (processed_clusters < number_of_clusters) {
            check_res = check_first_indirect(temp_inode, &processed_clusters, number_of_clusters, cluster_arr);
            if (check_res != 0) {
                break;
            }
        }

        //Kontrola druhých nepřímých
        if (processed_clusters < number_of_clusters) {
            check_res = check_second_indirect(temp_inode, &processed_clusters, number_of_clusters, cluster_arr);
            if (check_res != 0) {
                break;
            }
        }


    }

    for(i = 0; i < (*global_sb).cluster_count; i++){
        if(cluster_arr[i] == 1){
            printf("Lost reference cluster_id %d\n", i);
        }else if(cluster_arr[i] > 2){
            printf("Multiple inodes have reference to cluster %d\n", i);
        }
    }


    return CODE_OK;

}

/*
   Metoda vypíše informace ze superblocku.
 */
void utils_print_sb_info() {
    printf("*****************SB COMMAND_INFO*******************\n");
    printf("Hex bitmapi start addres: %x\n", (*global_sb).bitmapi_start_address);
    printf("Hex bitmap start address: %x\n", (*global_sb).bitmap_start_address);
    printf("Data start address: %x\n", (*global_sb).data_start_address);
    printf("Inode start address: %x \n", (*global_sb).inode_start_address);
    printf("Number of inodes %d\n", (*global_sb).inode_count);
    printf("Number of cluster %d \n", (*global_sb).cluster_count);
    printf("Number of free inodes %d\n", calc_number_of_free_inodes());
    printf("Number of free clusters %d\n", calc_number_of_free_clusters());
    printf("*******************************************\n");
}

