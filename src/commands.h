#ifndef MYFILESYSTEM_COMMANDS_H
#define MYFILESYSTEM_COMMANDS_H
#include "return_code.h"

#define COMMAND_COM_LEN 12
#define COMMAND_ARG_LEN 64

#define COMMAND_CP      "cp"
#define COMMAND_MV      "mv"
#define COMMAND_RM      "rm"
#define COMMAND_LS      "ls"
#define COMMAND_CD      "cd"
#define COMMAND_CAT     "cat"
#define COMMAND_PWD     "pwd"
#define COMMAND_EXIT    "exit"
#define COMMAND_INFO    "info"
#define COMMAND_LOAD    "load"
#define COMMAND_INCP    "incp"
#define COMMAND_HELP    "help"
#define COMMAND_OUTCP   "outcp"
#define COMMAND_MKDIR   "mkdir"
#define COMMAND_RMDIR   "rmdir"
#define COMMAND_CHECK   "check"
#define COMMAND_IINFO   "iinfo"
#define COMMAND_SBINFO  "sbinfo"
#define COMMAND_FORMAT  "format"
#define COMMAND_CORRUPT "corrupt"

/* Commands catchphrase */
#define COMMAND_OK                  "CODE_OK\n"
#define COMMAND_FILE_NAME_WRONG_LEN "FILE NAME IS TOO SHORT OR TOO LONG \n"
#define COMMAND_EXISTS              "EXISTS\n"
#define COMMAND_DIR_FULL            "DIRECTORY IS FULL.\n"
#define COMMAND_FILESYSTEM_FULL     "CANT CREATE DIR OR FILE. FILESYSTEM IS FULL.\n"
#define COMMAND_NOT_EMPTY           "NOT EMPTY\n"
#define COMMAND_FILE_NOT_FOUND      "FILE NOT FOUND\n"
#define COMMAND_PATH_NOT_FOUND      "PATH NOT FOUND\n"
#define COMMAND_WRONG_NAME          "INCORECT FILE NAME\n"
#define COMMAND_WRONG_TYPE          "WRONG DIR ITEM TYPE (FILE OR DIRECTORY)\n"
#define COMMAND_MISSING_ARG         "MISSING ARGUMENT\n"
#define COMMAND_CANNOT_CREATE_FILE  "CANNOT CREATE FILE\n"

/* ____________________________________________________________________________

   Prototypy funkcí

   ____________________________________________________________________________
 */

typedef struct thecommand{
    char command[COMMAND_COM_LEN];
    char arg1[COMMAND_ARG_LEN];
    char arg2[COMMAND_ARG_LEN];
} COMMAND;

return_code cp_func(char *source, char *dest);
return_code mv_func(char *source, char *dest);
return_code rm_func(char *path);
return_code ls_func(char *path);
return_code cd_func(char *dir_path);
return_code cat_func(char *path);
return_code pwd_func();
return_code info_func(char *path);
return_code load_func(char *path);
return_code incp_func(char *source, char *dest);
void help_func();
return_code mkdir_func(char *dir_location);
return_code rmdir_func(char *dir_location);
return_code outcp_funf(char *local_file, char *new_file);
return_code check_func();
void iinfo_func(char *inode_id);
void sbinfo_func();
return_code format_func(char *fs_size);
return_code corrupt_func(char *path);


#endif //MYFILESYSTEM_COMMANDS_H
