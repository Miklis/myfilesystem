//
// Created by miklis on 17.11.20.
//

#ifndef MYFILESYSTEM_FILESYSTEM_CACHE_H
#define MYFILESYSTEM_FILESYSTEM_CACHE_H
#include <stdio.h>

extern FILE *global_filesystem;
extern SUPER_BLOCK *global_sb;
extern DIR_ITEM *global_current_dir;
extern char *global_fs_name;

#endif //MYFILESYSTEM_FILESYSTEM_CACHE_H
