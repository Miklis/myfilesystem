#include <stdio.h>
#include "filesystem.h"
#include "command_executer.h"
#include "commands.h"


/*
 * ./myfs <fylesystem_dest_fold>
 *
 *
 */
int main(int argc, char *argv[]) {
    return_code result;

    if(argc == 2){
        filesystem_load_fs(argv[1]);
    }else{
        printf("Missing argumenet\n");
        return CODE_MISSING_ARGUMENT;
    }
    printf("WELCOME IN MY I-NODE FYLESYSTEM\n");


    char cmd_buff[COMMAND_BUFF_SIZE];

    while (1) {
        printf("MyFS>");
        fgets(cmd_buff, COMMAND_BUFF_SIZE, stdin);
        result = execute_command(cmd_buff);
        if(result == CODE_FS_FULL_DIR) {
            printf(COMMAND_DIR_FULL);
        }else if (result == CODE_MISSING_ARGUMENT){
            printf(COMMAND_MISSING_ARG);
        }else if(result == CODE_PATH_NOT_FOUND){
            printf(COMMAND_PATH_NOT_FOUND);
        }else if(result == CODE_FILE_NOT_FOUND){
            printf(COMMAND_FILE_NOT_FOUND);
        }else if(result == CODE_FS_FULL_FS){
            printf(COMMAND_FILESYSTEM_FULL);
        }else if(result == CODE_FS_SAME_NAMES){
            printf(COMMAND_EXISTS);
        }else if(result == CODE_NOT_EMPTY) {
            printf(COMMAND_NOT_EMPTY);
        }else if(result == CODE_COM_WRONG_F_NAME) {
            printf(COMMAND_WRONG_NAME);
        }else if (result == CODE_WRONG_TYPE){
            printf(COMMAND_WRONG_TYPE);
        }else if(result == CODE_EXIT_CODE){
            result = CODE_OK;
            break;
        }else if(result != CODE_OK){
            break;
        }
    }

    printf("Thank you for using my filesystem!\n");
    printf("Good bay\n");

    return result;
}
