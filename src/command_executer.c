#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "filesystem.h"
#include "commands.h"
#include "command_executer.h"
#include "filesystem_cache.h"


/*
    Porovnává dva stringy jestli jsou shodné na základě udané délky

    Proměnné:
            s1  - první porovnávaný
            s2  - druhý porovnávaný
            len - jak dlouhé má být porovnání

    Vrací bool:
                true  - shodné
                false - rozdílně
 */
bool compare_string(char *s1, char *s2, int len) {
    int i;
    if(strlen(s1) != strlen(s2)){
        return false;
    }

    for (i = 0; i < len; i++) {
        if(s1[i] != s2[i]){
            return false;
        }
    }

    return true;
}


/*
   Metoda zpracuje příkaz na základě předaného inputu a vrátí jeho typovou podobu

   Proměnné:
            input     - předaný příkaz v textové podobě
            input_len - délka vstupu
            out_cmd   - výsledný příkaz v typové podobě. Vrací se přes pointer.

 */
void process_user_input(char *input, int input_len,COMMAND *out_cmd){
    int i, word_ind = 0, char_ind = 0;
    char buff[input_len];

    memset((*out_cmd).command, 0, COMMAND_COM_LEN);
    memset((*out_cmd).arg1, 0, COMMAND_ARG_LEN);
    memset((*out_cmd).arg2, 0, COMMAND_ARG_LEN);

    for(i = 0; i < input_len; i++){
        buff[char_ind] = input[i];

        if(buff[char_ind] == ' ' || buff[char_ind] == '\n'){
            if(word_ind == 0){
                strncpy((*out_cmd).command, buff, char_ind + 1);
                (*out_cmd).command[char_ind] = '\0';
            }else if(word_ind == 1){
                strncpy((*out_cmd).arg1, buff, char_ind + 1);
                (*out_cmd).arg1[char_ind] = '\0';
            }else if(word_ind == 2){
                strncpy((*out_cmd).arg2, buff, char_ind + 1);
                (*out_cmd).arg2[char_ind] = '\0';
            }
            if(buff[char_ind] == '\n'){
                break;
            }

            char_ind = 0;
            word_ind++;
        }else{
            char_ind++;
        }
    }
}

/*
   Metoda zpracuje a vykoná příkaz na základě předaného inputu

   Proměnné:
            input - předaný příkaz v textové podobě

   Vrací return_code:
                        CODE_OK               - Příkaz byl vykonán v pořádku
                        CODE_MISSING_ARGUMENT - Chybí argument v příkazu
                        CODE_FATAL_ERROR      - Došlo k závažné chybě pří práci v paměti
                        CODE_FS_LOAD_FAILURE  - Nepodařilo se načíst filesystém
                        CODE_FS_ERROR         - Chyba ve fylesystému
                        CODE_FS_FULL_FS       - Fylesystém je plný
                        CODE_FS_FULL_DIR      - Adresář je plný
                        CODE_FS_OUT_OF_I      - Všechny Inody jsou zabrané
                        CODE_FS_SAME_NAMES    - Stejné jména filů
                        CODE_COM_WRONG_F_NAME -
                        CODE_PATH_NOT_FOUND   - Neexistující cesta
                        CODE_FILE_NOT_FOUND   - Neexistující soubor
                        CODE_NOT_EMPTY        - adresář není prázdný
                        CODE_EXIT_CODE        - signál pro ukončení filesystému
 */
return_code execute_command(char *input){
    return_code result = CODE_OK;
    COMMAND *cmd = malloc(sizeof(COMMAND));
    memset(cmd->arg1, '\0', COMMAND_ARG_LEN);
    memset(cmd->arg2, '\0', COMMAND_ARG_LEN);
    memset(cmd->command, '\0', COMMAND_COM_LEN);



    process_user_input(input, COMMAND_BUFF_SIZE, cmd);


    if(compare_string(cmd->command, COMMAND_FORMAT, 6) == true){
        result = format_func(cmd->arg1);
        free(cmd);
        return result;
    } else if (compare_string(cmd->command, COMMAND_EXIT, 4)) {
        filesystem_close();
        result = CODE_EXIT_CODE;
        free(cmd);
        return result;
    }

    if(global_filesystem != NULL){
        if (compare_string(cmd->command, COMMAND_CP, 2) == true) {
            result = cp_func(cmd->arg1, cmd->arg2);
        } else if (compare_string(cmd->command, COMMAND_MV, 2) == true) {
            result = mv_func(cmd->arg1, cmd->arg2);
        } else if (compare_string(cmd->command, COMMAND_RM, 2)) {
            result = rm_func(cmd->arg1);
        } else if (compare_string(cmd->command, COMMAND_LS, 2)) {
            result = ls_func(cmd->arg1);
        } else if (compare_string(cmd->command, COMMAND_CD, 2)) {
            result = cd_func(cmd->arg1);
        } else if (compare_string(cmd->command, COMMAND_CAT, 3)) {
            result = cat_func(cmd->arg1);
        } else if (compare_string(cmd->command, COMMAND_PWD, 3)) {
            result = pwd_func();
        } else if (compare_string(cmd->command, COMMAND_INFO, 4)) {
            result = info_func(cmd->arg1);
        } else if (compare_string(cmd->command, COMMAND_LOAD, 4)) {
            result = load_func("/home/miklis/CLionProjects/MyFileSystem/cmake-build-debug/comm.load");
        } else if (compare_string(cmd->command, COMMAND_INCP, 4)) {
            result = incp_func(cmd->arg1, cmd->arg2);
        }else if(compare_string(cmd->command, COMMAND_HELP, 4)){
            help_func();
        } else if (compare_string(cmd->command, COMMAND_OUTCP, 5)) {
            result = outcp_funf(cmd->arg1, cmd->arg2);
        }else if (compare_string(cmd->command, COMMAND_MKDIR, 5)) {
            result = mkdir_func(cmd->arg1);
        }else if (compare_string(cmd->command, COMMAND_RMDIR, 5)) {
            result = rmdir_func(cmd->arg1);
        }else if (compare_string(cmd->command, COMMAND_CHECK, 5)) {
            result =  check_func();
        }else if(compare_string(cmd->command, COMMAND_IINFO, 5)) {
            iinfo_func(cmd->arg1);
        }else if(compare_string(cmd->command, COMMAND_SBINFO, 6)) {
            sbinfo_func();
        }else if(compare_string(cmd->command, COMMAND_CORRUPT, 7)){
            result = corrupt_func(cmd->arg1);
        }else{
            printf("This is not global_filesystem function! Try it again :)\n");
        }
    }else{
        printf("Format filesystem please.\n");
    }
    free(cmd);

    return result;
}
