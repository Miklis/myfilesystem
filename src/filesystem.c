#include <memory.h>
#include <unistd.h>
#include "filesystem.h"
#include "filesystem_cache.h"
#include "fs_utils.h"


/*
   Metoda na základě předané hodnoty určí celkou velikost filesystému.
   Akceptované hodnoty jsou např 1K, 20M. Číslo by mělo být v rozmezí 1-1000.

   Proměnné:
            fs_size - velikost filesystému skládající se z <číslo><velikost (K | M)>

   Vrací int32_t, tedy výslednou velikost fylesystému.
 */
int32_t calc_fs_size(char *fs_size) {
    int32_t fs_size_in_bytes;
    int32_t number_size_of_fs_size = strlen(fs_size) - 1; /* ukončovací znak a značka K (kilobyte) nebo M (megabyte) */
    int size_of_fs = (int) strtol(fs_size, (char **) NULL, 10);

    char *fs_size_number = NULL;
    char size = fs_size[(strlen(fs_size) - 1)];

    /* strncpy(fs_size_number, fs_size, number_size_of_fs_size); */

    /* printf("%c\n", size); */
    if (size == 'M') {
        fs_size_in_bytes = size_of_fs * 1024 * 1024;
    } else if (size == 'K') {
        fs_size_in_bytes = size_of_fs * 1024;
    } else {
        printf("Unknown size unit!!! Permit unit M - megabyte, K - kilobyte\n");
        return -1;
    }

    return fs_size_in_bytes;
}


/*
   Metoda získá login aktuálně přihlášeného uživatele

   Proměnné:
            buff - buffer do kterého se uloží načtený login

   Vrací int, který nabývá hodnoty 0, pokud proběhlo vše v pořádku nebo -1, kdy se stala chyba.
 */
int get_loggedUser(char *buff) {
    int return_val;

    return_val = getlogin_r(buff, MAX_USERNAME_LEN);

    if (return_val == 0) {
        return 0;
    } else {
        return -1;
    }

}

/*
   Metoda vytváří kořenový adresář filesystému. Výsledný odkaz na adresář se ukládá do globální proměnné.
   global_cur_dir.

   Vrací return_code:
                    CODE_OK              - Soubor byl vytvořen v pořádku
                    CODE_FATAL_ERROR     - Chyba při práci s pamětí
                    CODE_FS_ERROR        - Došlo k chybě při operaci s filesystémem
 */
return_code create_root_directory() {
    int inode_id, needed_clusters;
    DIR_ITEM *root_dir_dot, *root_dir_double_dot;
    INODE *inode;
    int32_t *free_clusters;

    /* Alokace paměti */
    inode = calloc(sizeof(INODE), 1);
    root_dir_dot = calloc(sizeof(DIR_ITEM), 1);
    root_dir_double_dot = calloc(sizeof(DIR_ITEM), 1);

    /* Získání id pro inode */
    inode_id = utils_find_first_free_inode();

    /* Výpočet kolik bude potřeba clusterů */
    //needed_clusters = ceil((double)(sizeof(DIR_ITEM) * 2) / CLUSTER_SIZE);
    needed_clusters = 1; /* Předpoklad že soubory budou ukládány pouze do jednoho clusteru a použije se jen jedna reference u Inodu */
    free_clusters = malloc(sizeof(int32_t) * needed_clusters);

    if (inode == NULL || root_dir_dot == NULL || root_dir_double_dot == NULL ||
        free_clusters == NULL) { /* Nedostatek ram paměti */
        return CODE_FATAL_ERROR;
    }

    if (inode_id == -1) {
        return CODE_FS_ERROR;
    }

    if (find_free_clusters(&free_clusters, needed_clusters) == -1) {
        return CODE_FS_ERROR;
    }

    inode->nodeid = inode_id;
    inode->isDirectory = true;
    inode->direct1 = free_clusters[0];
    inode->file_size = CLUSTER_SIZE;

    root_dir_dot->inode = inode->nodeid;
    strncpy(root_dir_dot->item_name, ".\0", 4);
    utils_modify_inode_references(inode, true);

    root_dir_double_dot->inode = inode->nodeid;
    strncpy(root_dir_double_dot->item_name, "..\0", 4);
    utils_modify_inode_references(inode, true);

    fs_seek_set_i(inode_id);

    fwrite(inode, sizeof(INODE), 1, global_filesystem);
    fs_seek_set_c(inode->direct1);
    fwrite(root_dir_dot, sizeof(DIR_ITEM), 1, global_filesystem);
    fwrite(root_dir_double_dot, sizeof(DIR_ITEM), 1, global_filesystem);

    utils_copy_dir_item(root_dir_dot, global_current_dir);
    utils_print_sb_info();
    free(root_dir_dot);
    free(free_clusters);
    free(root_dir_double_dot);
    free(inode);
    return CODE_OK;
}


/*
   Metoda inicializuje superblock a výslednou strukturu ukládá na začátek souboru.

   Proměnné:
            username  - jméno přihlášeného uživatele
            fs_size   - velikost filesystému

   Vrací return_code:
                    CODE_OK              - Vše proběhlo v pořádku
                    CODE_FS_ERROR        - Snaha zapsat na již použitý bit
                    CODE_FS_LOAD_FAILURE - Filesystém se nepodařilo vytvořit
 */
return_code init_superblock(char *username, char *fs_size) {
    int32_t fs_size_num = calc_fs_size(fs_size);
    int32_t number_of_inodes = 0;
    int32_t bytes_used_for_inode_bitmap = 0;
    int32_t bytes_used_for_cluster_bitmap = 0;
    int32_t number_of_data_clusters = 0;
    uint8_t *inode_bitmap;
    uint8_t *cluster_bitmap;

    if (fs_size_num == -1) {
        return CODE_MISSING_ARGUMENT;
    }

    if(global_sb == NULL){
        global_sb = calloc(sizeof (SUPER_BLOCK), 1);
    }

    if(global_current_dir == NULL){
        global_current_dir = calloc(sizeof(DIR_ITEM), 1);
    }


    fseek(global_filesystem, fs_size_num - 1, SEEK_SET); /* Skočí na konec souboru */
    fputc('\0', global_filesystem);                       /* Zapíše nulu na konec */
    fseek(global_filesystem, 0, SEEK_SET);                /* Skočí zpět na začátek */

    number_of_inodes = calc_number_of_inodes(fs_size_num);
    bytes_used_for_inode_bitmap = calc_bytes_in_bitmap(number_of_inodes);
    number_of_data_clusters = calc_number_of_data_cluster(sizeof(SUPER_BLOCK), number_of_inodes, fs_size_num);


    inode_bitmap = calloc(sizeof(uint8_t), bytes_used_for_inode_bitmap);
    cluster_bitmap = calloc(sizeof(uint8_t), bytes_used_for_cluster_bitmap);

    strncpy(global_sb->signature, username, MAX_USERNAME_LEN);
    strncpy(global_sb->volume_descriptor, DESCRIPTOR, DESCRIPTOR_LEN);
    global_sb->disk_size = fs_size_num;
    global_sb->cluster_size = CLUSTER_SIZE;
    global_sb->cluster_count = number_of_data_clusters;
    global_sb->inode_count = number_of_inodes;
    global_sb->bitmapi_start_address = sizeof(SUPER_BLOCK);
    global_sb->bitmap_start_address = global_sb->bitmapi_start_address + bytes_used_for_inode_bitmap;
    global_sb->inode_start_address = global_sb->bitmap_start_address + number_of_data_clusters;
    global_sb->data_start_address = global_sb->inode_start_address + sizeof(INODE) * number_of_inodes;
    fwrite(global_sb, sizeof(SUPER_BLOCK), 1, global_filesystem);

    fs_seek_set(global_sb->bitmap_start_address);
    fwrite(cluster_bitmap, sizeof(uint8_t), bytes_used_for_cluster_bitmap, global_filesystem);
    fs_seek_set(global_sb->bitmapi_start_address);
    fwrite(inode_bitmap, sizeof(uint8_t), bytes_used_for_inode_bitmap, global_filesystem);

    free(cluster_bitmap);
    free(inode_bitmap);
    return CODE_OK;
}


/*
   Metoda načte již vytvořený filesystém ze souboru, kdy je nahrán superblock a kořenový adresář do globální
   proměnných.

   Proměnné:
            username   - jméno přihlášeného uživatele

   Vrací return_code:
                    CODE_OK              - Načtení proběhlo v pořádku
                    CODE_FATAL_ERROR     - Došlo k chybě v paměti
 */
return_code reload_fs(char *username) {
    INODE *root_dir_inode;


    global_sb = calloc(sizeof (SUPER_BLOCK), 1);
    global_current_dir = calloc(sizeof(DIR_ITEM), 1);
    root_dir_inode = malloc(sizeof(INODE));

    if(root_dir_inode == NULL){
        return CODE_FATAL_ERROR;
    }

    fread(global_sb, sizeof(SUPER_BLOCK), 1, global_filesystem);
    strncpy(global_sb->signature, username, MAX_USERNAME_LEN);

    utils_load_inode(root_dir_inode, 1);
    fs_seek_set_c(root_dir_inode->direct1);
    fread(global_current_dir, sizeof(DIR_ITEM), 1, global_filesystem);
    free(root_dir_inode);
    return CODE_OK;
}

/*
   Metoda naformátuje filesystém na zadanou velikost

   Proměnné:
            fs_size  - velikost jakou má mít filesystém

   Vrací return_code:
                    CODE_OK              - Soubor byl v inodu nalezen
                    CODE_FS_LOAD_FAILURE - Chyba při formátování filesystému
                    CODE_FS_ERROR        - Chyba při práci s filesystémem
 */
return_code filesystem_format_fs(char *fs_size) {
    char username[MAX_USERNAME_LEN];
    get_loggedUser(username);
    return_code result = CODE_OK;

    if (access(global_fs_name, F_OK) == 0) {
        if (remove(global_fs_name) != 0) {
            return CODE_FS_LOAD_FAILURE;
        }else{
            fclose(global_filesystem);
            global_filesystem = NULL;
        }
    }

    if(global_filesystem == NULL){
        global_filesystem = fopen(global_fs_name, "wb+");
    }

    result = init_superblock(username, fs_size);
    if (result == CODE_OK) {
        create_root_directory();
    }
    return result;
}


/*
   Metoda po startu načítá filesystém v případě, že již existuje soubor který má filesystém obsahovat.
   Nebo metoda vytvoří jen základní globalní proměnné a systém se bude muset ještě naformátova

   Proměnné:
            fs_file  - jméno filu kde má být filesystém

   Vrací return_code:
                    CODE_OK              - Soubor byl v inodu nalezen
                    CODE_FATAL_ERROR     - Došlo k chybě v paměti
 */
return_code filesystem_load_fs(char *fs_file) {
    global_fs_name = calloc(sizeof(char), MAX_PATH_LEN);
    char username[MAX_USERNAME_LEN];
    get_loggedUser(username);

    if (global_fs_name == NULL) {
        return CODE_FATAL_ERROR;
    } else {
        strncpy(global_fs_name, fs_file, strlen(fs_file));
        if (access(fs_file, F_OK) == 0) {
            global_filesystem = fopen(fs_file, "rb+");
            return reload_fs(username);
        } else {
            global_filesystem = NULL;
        }
    }
    return CODE_OK;
}


/*
   Metoda pro uzavření práce s filesystémem. Uvolní všechny globální proměnné a zavře globální proud pro práci
   se souborem, kde je filesystém.
 */
void filesystem_close() {
    free(global_current_dir);
    free(global_sb);
    free(global_fs_name);
    if(global_filesystem != NULL){
        fclose(global_filesystem);
    }

}



