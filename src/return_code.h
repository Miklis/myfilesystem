//
// Created by miklis on 17.11.20.
//

#ifndef MYFILESYSTEM_RETURN_CODE_H
#define MYFILESYSTEM_RETURN_CODE_H

typedef enum {
    CODE_OK                          = 0,
    CODE_MISSING_ARGUMENT            = 1,
    CODE_FATAL_ERROR                 = 2,
    CODE_FS_LOAD_FAILURE             = 3,
    CODE_FS_ERROR                    = 4,
    CODE_FS_FULL_FS                  = 5,
    CODE_FS_FULL_DIR                 = 6,
    CODE_FS_OUT_OF_I                 = 7,
    CODE_FS_SAME_NAMES               = 8,
    CODE_COM_WRONG_F_NAME            = 9,
    CODE_PATH_NOT_FOUND              = 10,
    CODE_FILE_NOT_FOUND              = 11,
    CODE_NOT_EMPTY                   = 12,
    CODE_WRONG_TYPE                  = 13,
    CODE_EXIT_CODE                   = 14
}return_code;

#endif //MYFILESYSTEM_RETURN_CODE_H
