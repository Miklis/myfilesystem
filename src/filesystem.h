#ifndef MYFILESYSTEM_FILESYSTEM_H
#define MYFILESYSTEM_FILESYSTEM_H
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include "return_code.h"

#define MAX_PATH_LEN     256
#define MAX_FILE_NAME    12
#define CLUSTER_SIZE     64
#define MAX_USERNAME_LEN 9
#define DESCRIPTOR_LEN   251
#define INODE_PERC       0.15
#define DESCRIPTOR       "testovací fs"

/*const int32_t ID_ITEM_FREE = 0; *//* Takto označené inody značí že jsou volné */


typedef struct superblock SUPER_BLOCK;
typedef struct pseudo_inode INODE;
typedef struct directory_item DIR_ITEM;



struct superblock {
    char signature[MAX_USERNAME_LEN];         /* login autora FS */
    char volume_descriptor[DESCRIPTOR_LEN];   /* popis vygenerovaného FS */
    int32_t disk_size;                        /* celkova velikost VFS */
    int32_t cluster_size;                     /* velikost clusteru */
    int32_t cluster_count;                    /* pocet clusteru */
    int32_t inode_count;                      /* počet inodu */
    int32_t bitmapi_start_address;            /* adresa pocatku bitmapy i-uzlů */
    int32_t bitmap_start_address;             /* adresa pocatku bitmapy datových bloků */
    int32_t inode_start_address;              /* adresa pocatku  i-uzlů */
    int32_t data_start_address;               /* adresa pocatku datovych bloku */
};

/*
 *V directech jsou uložená čísla clusterů na které ukazují a ne jejich adresy.
 */
struct pseudo_inode {
    int32_t nodeid;                 /* ID i-uzlu, pokud ID = ID_ITEM_FREE, je polozka volna */
    bool isDirectory;               /* soubor, nebo adresar */
    int8_t references;              /* počet odkazů na i-uzel, používá se pro hardlinky */
    int32_t file_size;              /* velikost souboru v bytech */
    int32_t direct1;                /* 1. přímý odkaz na datové bloky */
    int32_t direct2;                /* 2. přímý odkaz na datové bloky */
    int32_t direct3;                /* 3. přímý odkaz na datové bloky */
    int32_t direct4;                /* 4. přímý odkaz na datové bloky */
    int32_t direct5;                /* 5. přímý odkaz na datové bloky */
    int32_t indirect1;              /* 1. nepřímý odkaz (odkaz - datové bloky) */
    int32_t indirect2;              /* 2. nepřímý odkaz (odkaz - odkaz - datové bloky) */
};


struct directory_item {
    int32_t inode;                   /* inode odpovídající souboru */
    char item_name[MAX_FILE_NAME];              /* 8+3 + /0 C/C++ ukoncovaci string znak */
};

return_code filesystem_format_fs(char *fs_size);
return_code filesystem_load_fs(char *fs_file);
void filesystem_close();

#endif