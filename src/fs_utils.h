#ifndef MYFILESYSTEM_FS_UTILS_H
#define MYFILESYSTEM_FS_UTILS_H
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include "filesystem.h"

FILE *global_filesystem;
SUPER_BLOCK *global_sb;
DIR_ITEM *global_current_dir;
char *global_fs_name;
char  current_path[256];

int32_t calc_number_of_inodes(int32_t fs_size);
int32_t calc_bytes_in_bitmap(int req_bits);
int32_t calc_number_of_inodes(int32_t fs_size);
int32_t calc_number_of_data_cluster(int sb_size, int inodes_num, int fs_size);
void fs_seek_set(int32_t offset);
void fs_seek_set_i(int32_t inode_id);
void fs_seek_set_c(int32_t cluster_id);
void utils_modify_inode_references(INODE *inode, bool op);
int utils_find_first_free_inode();
int calc_number_of_free_clusters();
int find_free_clusters(int32_t **out_free_clusters, int number_of_needed_clusters);
int utils_calc_needed_cluster_for_file(int size);
return_code utils_check_dir_items_names(DIR_ITEM *directory, char *name, int *out_items_in_directory);
return_code utils_has_dir_space(DIR_ITEM *dir, int32_t *offset, int *has_space);
return_code utils_write_file_reference_to_dir(DIR_ITEM *parent, DIR_ITEM *dir_item, int32_t *offset);
return_code utils_is_directory_empty(DIR_ITEM *directory, int *out_is_empty);
return_code utils_get_dir_item(DIR_ITEM *directory, char *item_name, DIR_ITEM *out_dir_item);
return_code utils_get_inode_item(INODE *inode, char *item_name, DIR_ITEM *out_dir_item);
return_code utils_process_path(char *path, DIR_ITEM *out_dir_item);
return_code utils_find_dir_level_up(DIR_ITEM *cur_dir, DIR_ITEM *out_dir_level_up);
return_code utils_find_cur_dir_level_up(DIR_ITEM *cur_dir);
return_code utils_remove_dir_reference(INODE *dir_inode, INODE *parrent_dir_inode, char *file_name, bool remove_inode);
return_code utils_check_inodes_consistency();
void utils_remove_inode(INODE *dir_inode);
void utils_print_sb_info();
void utils_print_inode_info(int inode_id);
void utils_load_inode(INODE *out_inode, int node_id);
void utils_write_byte_to_links(INODE *inode, char *data, int cluster, int bytes);
void utils_read_from_links(INODE *inode, int cluster, int byte_num_to_read, char *out_data);
void utils_assign_clusters_to_inode(INODE *inode, int32_t *free_clusters, int cluster_num);
void utils_copy_dir_item(DIR_ITEM *source, DIR_ITEM *dest);
void utils_free_cluster(int cluster_id);
void utils_free_inode(int inode_id);
void utils_intersect_directory_path_and_file_name(char *source, char *out_dir_path, char *out_file_name);



#endif
